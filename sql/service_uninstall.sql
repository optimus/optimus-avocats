
DROP TABLE IF EXISTS `common`.`diligences_categories`;
DROP TABLE IF EXISTS `common`.`diligences_subcategories`;
DROP TABLE IF EXISTS `common`.`dossiers_domaines`;
DROP TABLE IF EXISTS `common`.`dossiers_sousdomaines`;
DROP TABLE IF EXISTS `common`.`intervenants_qualites`;
DROP TABLE IF EXISTS `common`.`juridictions`;
DROP TABLE IF EXISTS `common`.`communes_to_juridictions`;