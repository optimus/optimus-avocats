ALTER TABLE IF EXISTS `dossiers_diligences` CHANGE `categorie` `category` tinyint(3) unsigned DEFAULT 1 NULL;
ALTER TABLE IF EXISTS `dossiers_diligences` CHANGE `coefficient` `quantity` decimal(10,2) DEFAULT 0.00 NULL;
ALTER TABLE IF EXISTS `dossiers_diligences` CHANGE `tarif` `price` decimal(10,2) DEFAULT 0.00 NULL;
ALTER TABLE IF EXISTS `dossiers_diligences` CHANGE `facture_serveur` `invoice_server` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL NULL;
ALTER TABLE IF EXISTS `dossiers_diligences` CHANGE `facture_structure` `invoice_owner` int(10) unsigned DEFAULT NULL NULL;
ALTER TABLE IF EXISTS `dossiers_diligences` CHANGE `facture_id` `invoice_id` int(10) unsigned DEFAULT NULL NULL;
ALTER TABLE IF EXISTS `dossiers_diligences` CHANGE `facture_numero` `invoice_number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL NULL;
ALTER TABLE IF EXISTS `dossiers_diligences` ADD COLUMN `invoice_business_displayname` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL NULL AFTER `price`;