ALTER TABLE IF EXISTS `dossiers_intervenants` DROP COLUMN `qualite`;
ALTER TABLE IF EXISTS `dossiers_intervenants` DROP COLUMN `lien`;
ALTER TABLE IF EXISTS `dossiers_intervenants` ADD CONSTRAINT `intervenants_to_dossiers` FOREIGN KEY (`dossier`) REFERENCES `dossiers` (`id`) ON DELETE CASCADE;
ALTER TABLE IF EXISTS `dossiers_intervenants` ADD CONSTRAINT `intervenants_to_contacts` FOREIGN KEY (`contact`) REFERENCES `contacts` (`id`) ON DELETE CASCADE;

CREATE TABLE IF NOT EXISTS `dossiers_intervenants_qualites` (
	`intervenant` int(10) unsigned NOT NULL,
	`qualite` tinyint(4) unsigned NOT NULL,
	PRIMARY KEY (`intervenant`,`qualite`) USING BTREE,
	KEY `qualites_to_intervenants` (`intervenant`),
	CONSTRAINT `qualites_to_intervenants` FOREIGN KEY (`intervenant`) REFERENCES `dossiers_intervenants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `dossiers_intervenants_mandataires` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`intervenant` int(10) unsigned NOT NULL,
	`contact` int(10) unsigned NOT NULL,
	`type` tinyint(4) unsigned NOT NULL,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `mandataires_to_intervenants` (`intervenant`),
	CONSTRAINT `mandataires_to_intervenants` FOREIGN KEY (`intervenant`) REFERENCES `dossiers_intervenants` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;