CREATE TABLE IF NOT EXISTS `dossiers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `numero` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `date_ouverture` date DEFAULT NULL,
  `date_classement` date DEFAULT NULL,
  `numero_archive` int(10) unsigned DEFAULT NULL,
  `domaine` smallint(5) unsigned DEFAULT 0,
  `sous_domaine` smallint(5) unsigned DEFAULT 0,
  `conseil` bit(1) NOT NULL DEFAULT b'0',
  `contentieux` bit(1) NOT NULL DEFAULT b'0',
  `aj` bit(1) NOT NULL DEFAULT b'0',
  `notes` text DEFAULT NULL,
  `rg` varchar(8) DEFAULT NULL,
  `portalis` varchar(16) DEFAULT NULL,
  `parquet` varchar(16) DEFAULT NULL,
  `instruction` varchar(16) DEFAULT NULL,
  `displayname` varchar(128) GENERATED ALWAYS AS (coalesce(nullif(ifnull(`nom`,''),''),concat('Dossier ', id))) VIRTUAL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `dossiers_diligences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dossier` int(10) unsigned DEFAULT NULL,
  `date` date DEFAULT current_timestamp(),
  `intervenant` int(10) unsigned DEFAULT NULL,
  `beneficiaire` int(10) unsigned DEFAULT NULL,
  `commission` decimal(2,2) DEFAULT 0.00,
  `description` text DEFAULT NULL,
  `categorie` tinyint(3) unsigned DEFAULT 1,
  `type` tinyint(3) unsigned DEFAULT 1,
  `coefficient` decimal(10,2) DEFAULT 0.00,
  `tarif` decimal(10,2) DEFAULT 0.00,
  `facture_serveur` varchar(128) DEFAULT NULL,
  `facture_structure` int(10) unsigned DEFAULT NULL,
  `facture_id` int(10) unsigned DEFAULT NULL,
  `facture_numero` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `dossiers_intervenants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dossier` int(10) unsigned NOT NULL,
  `contact` int(10) unsigned NOT NULL,
  `qualite` smallint(5) unsigned NOT NULL,
  `lien` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;