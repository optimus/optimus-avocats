
USE `common`;

DROP TABLE IF EXISTS `intervenants_mandataires_types`;
CREATE TABLE IF NOT EXISTS `intervenants_mandataires_types` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
INSERT INTO `intervenants_mandataires_types` VALUES (10,"Avocat");
INSERT INTO `intervenants_mandataires_types` VALUES (20,"Avocat Plaidant");
INSERT INTO `intervenants_mandataires_types` VALUES (30,"Avocat Postulant");
INSERT INTO `intervenants_mandataires_types` VALUES (60,"Commissaire de justice");
INSERT INTO `intervenants_mandataires_types` VALUES (70,"Notaire");
INSERT INTO `intervenants_mandataires_types` VALUES (80,"Expert Amiable");
INSERT INTO `intervenants_mandataires_types` VALUES (90,"Expert Judiciaire");
INSERT INTO `intervenants_mandataires_types` VALUES (120,"Administrateur Judiciaire");
INSERT INTO `intervenants_mandataires_types` VALUES (125,"Mandataire Judiciaire");
INSERT INTO `intervenants_mandataires_types` VALUES (130,"Liquidateur Judiciaire");
INSERT INTO `intervenants_mandataires_types` VALUES (135,"Liquidateur Amiable");
INSERT INTO `intervenants_mandataires_types` VALUES (150,"Curateur");
INSERT INTO `intervenants_mandataires_types` VALUES (160,"Tuteur");
INSERT INTO `intervenants_mandataires_types` VALUES (180,"Représentant légal");
INSERT INTO `intervenants_mandataires_types` VALUES (190,"Gérant");
INSERT INTO `intervenants_mandataires_types` VALUES (200,"Président");
INSERT INTO `intervenants_mandataires_types` VALUES (210,"Directeur Général");
INSERT INTO `intervenants_mandataires_types` VALUES (230,"Apporteur d'affaires");

DROP TABLE IF EXISTS `intervenants_qualites`;
CREATE TABLE IF NOT EXISTS `intervenants_qualites` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
INSERT INTO `intervenants_qualites` VALUES (10,"Client");
INSERT INTO `intervenants_qualites` VALUES (20,"Adversaire");
INSERT INTO `intervenants_qualites` VALUES (30,"Demandeur");
INSERT INTO `intervenants_qualites` VALUES (40,"Défendeur");
INSERT INTO `intervenants_qualites` VALUES (50,"Intervenant volontaire");
INSERT INTO `intervenants_qualites` VALUES (60,"Intervenant forcé");
INSERT INTO `intervenants_qualites` VALUES (70,"Appelant");
INSERT INTO `intervenants_qualites` VALUES (80,"Intimé");
INSERT INTO `intervenants_qualites` VALUES (90,"Appelé en déclaration de jugement commun");
INSERT INTO `intervenants_qualites` VALUES (100,"Juridiction");
INSERT INTO `intervenants_qualites` VALUES (110,"Cocontractant");