USE `common`;

INSERT INTO `intervenants_qualites` VALUES (120,"Prévenu");
INSERT INTO `intervenants_qualites` VALUES (130,"Mis en examen");
INSERT INTO `intervenants_qualites` VALUES (140,"Témoin assisté");
INSERT INTO `intervenants_qualites` VALUES (150,"Partie civile");
INSERT INTO `intervenants_qualites` VALUES (160,"Adopté");
INSERT INTO `intervenants_qualites` VALUES (170,"Adoptant");
INSERT INTO `intervenants_qualites` VALUES (180,"Bailleur");
INSERT INTO `intervenants_qualites` VALUES (190,"Locataire");