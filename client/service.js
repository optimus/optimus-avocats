export default class OptimusAvocatsServices
{
	constructor()
	{
		this.name = 'optimus-avocats'
		this.resources = [
			{
				id: 'dossiers',
				name: 'dossiers',
				description: 'Tous les dossiers',
				path: 'dossiers'
			},
			{
				id: 'dossier',
				name: 'dossier',
				description: 'Un dossier déterminé',
				path: 'dossiers/id',
				endpoint: '/optimus-avocats/{owner}/dossiers'
			}
		]
		this.swagger_modules =
			[
				{
					id: "optimus-avocats",
					title: "OPTIMUS AVOCATS",
					path: "/services/optimus-avocats/optimus-avocats.yaml",
					filters: ["constants", "dossiers", "diligences", "intervenants", "huissiers", "service", "translations"]
				},
			]
		this.optimus_contacts_editor_tabs =
			[
				{
					id: "tab_huissiers",
					text: "Huissiers",
					link: "/services/optimus-avocats/contacts/huissiers.js",
					position: 450
				},
				{
					id: "tab_juridictions",
					text: "Juridictions",
					link: "/services/optimus-avocats/contacts/juridictions.js",
					position: 451
				},
				{
					id: "tab_dossiers",
					text: "Dossiers",
					link: "/services/optimus-avocats/contacts/dossiers.js",
					position: 675
				}
			]
		this.optimus_calendar_event_editor_tabs =
			[
				{
					id: "calendar_tab_dossier",
					text: "Dossier lié",
					link: "/services/optimus-avocats/calendars/event_modal_dossier_tab.js",
					position: 350
				},
			]
		store.services.push(this)
	}

	login()
	{
		leftmenu.create('optimus-avocats', 'AVOCATS')
		leftmenu['optimus-avocats'].add('dossiers', 'Dossiers', 'fas fa-folder', () => router('optimus-avocats/dossiers'))

		leftmenu.create('optimus-avocats-tools', 'OUTILS AVOCATS')
		leftmenu['optimus-avocats-tools'].add('societe-ninja', 'societe.ninja', 'fas fa-building', () => window.open('https://www.societe.ninja'))
		leftmenu['optimus-avocats-tools'].add('jurisprudence-ninja', 'jurisprudence.ninja', 'fas fa-gavel', () => window.open('https://www.jurisprudence.ninja'))
		leftmenu['optimus-avocats-tools'].add('calcul-ninja', 'calcul.ninja', 'fas fa-calculator', () => window.open('https://www.calcul.ninja'))
		leftmenu['optimus-avocats-tools'].add('optimus-ai', 'Optimus AI (GPT4)', 'fas fa-robot', () => router('optimus-avocats/optimus-ai'))
		leftmenu['optimus-avocats-tools'].add('jitsi', 'Visioconférence', 'fas fa-video', () => router('optimus-avocats/jitsi'))

		setTimeout(() =>
		{
			if (leftmenu['optimus-business']?.querySelector('#businesses'))
				leftmenu['optimus-business'].querySelector('#businesses > a > span').innerText = "Mon cabinet"
		}, 100)

		store.resources = store.resources.concat(this.resources)
	}

	logout()
	{
		leftmenu['optimus-avocats'].remove()
		leftmenu['optimus-avocats-tools'].remove()

		for (let resource of this.resources)
			store.resources.splice(store.resources.findIndex(item => item.id == resource.id), 1)

		store.services = store.services.filter(service => service.name != this.id)
	}

	async global_search(search_query)
	{
		return [
			{
				icon: 'fas fa-folder',
				title: 'Dossiers',
				data: rest(store.user.server + '/optimus-avocats/' + store.user.id + '/dossiers', 'GET', { filter: [{ field: "*", type: "like", value: search_query }], references: true, size: 12, page: 1 })
					.then(dossiers => dossiers?.data?.map(dossier => { return { displayname: dossier.displayname, route: 'optimus-avocats/dossiers/editor?owner=' + store.user.id + '&id=' + dossier.id + '#general' } }))
			}
		]
	}

	dashboard()
	{
		//quickactions_add('quickaction-avocats-header', 'header', 'AVOCATS')
		//quickactions_add('quickaction-avocats-dossiers-create', 'item', 'Créer un nouveau dossier', 'fas fa-folder-plus', () => quickactionDossierCreate())
		//quickactions_add('quickaction-avocats-dossiers-note-create', 'item', 'Ajouter une note dans un dossier', 'fas fa-note-sticky', () => quickactionDossierNoteCreate())
		//quickactions_add('quickaction-avocats-dossiers-diligence-create', 'item', 'Ajouter une diligence dans un dossier', 'fas fa-table-list', () => quickactionDossierDiligenceCreate())
	}
}