export default class OptimusAvocatsDossiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		load('/components/optimus_table/index.js', this.target,
			{
				id: 'dossiers',
				version: 2,
				title: 'Dossiers',
				resource: 'dossiers',
				addIcon: 'fa-folder-plus',
				url: 'https://' + store.user.server + '/optimus-avocats/owner/dossiers',
				striped: true,
				bordered: true,
				columnSeparator: true,
				fullscreen: true,
				tabulator:
				{
					progressiveLoad: 'auto',
					columns:
						[
							{ field: "id", title: 'ID', sorter: 'number', sorterParams: { type: 'integer' } },
							{ field: 'displayname', title: 'Nom', width: 350 },
							{ field: 'numero', title: 'N°', width: 90 },
							{ field: 'date_ouverture', title: 'Ouverture', sorter: 'date', sorterParams: { type: 'date' }, mutator: value => value ? new Date(value).toLocaleDateString('fr') : null, hozAlign: 'center' },
							{ field: 'date_classement', title: 'Classement', sorter: 'date', sorterParams: { type: 'date' }, mutator: value => value ? new Date(value).toLocaleDateString('fr') : null, hozAlign: 'center' },
							{ field: 'numero_archive', title: 'Archive', },
							{ field: 'domaine_description', title: 'Domaine' },
							{ field: 'sous_domaine_description', title: 'Sous domaine' },
							{ field: 'conseil', title: 'Conseil', formatter: "tickCross", formatterParams: { tickElement: "<i class='fa fa-check'></i>", crossElement: false }, hozAlign: "center" },
							{ field: 'contentieux', title: 'Contentieux', formatter: "tickCross", formatterParams: { tickElement: "<i class='fa fa-check'></i>", crossElement: false }, hozAlign: "center" },
							{ field: 'aj', title: 'AJ', formatter: "tickCross", formatterParams: { tickElement: "<i class='fa fa-check'></i>", crossElement: false }, hozAlign: "center" },
							{ field: 'rg', title: 'RG', width: 100 },
							{ field: 'portalis', title: 'N° Portalis', visible: false },
							{ field: 'parquet', title: 'N° Parquet', visible: false },
							{ field: 'instruction', title: 'N° Instruction', visible: false },
						],
				},
				rowClick: (event, row) => router('optimus-avocats/dossiers/editor?owner=' + (store.queryParams.owner || store.user.id) + '&id=' + row.getIndex() + '#general'),
				addClick: () =>
					rest(store.user.server + '/optimus-avocats/' + (store.queryParams.owner || store.user.id) + '/dossiers', 'POST', {}, 'toast')
						.then(data => data.code == 201 && router('optimus-avocats/dossiers/editor?owner=' + store.user.id + '&id=' + data.data.id + '#general'))
			})
	}
}