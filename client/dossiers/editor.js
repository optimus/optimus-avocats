export default class OptimusAvocatsDossiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		loader(this.target)
		let response = await rest('https://' + this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id)
		if (response.code == 404)
			return load('/404.html', main) && loader(this.target, false)
		await load('/services/optimus-avocats/dossiers/editor.html', main)
		main.querySelector('.title').innerText = response.data.displayname.toUpperCase()

		if (response.data?.restrictions?.includes('delete'))
			this.target.querySelector('.delete-button').classList.add('is-hidden')

		let tabs =
			[
				{
					id: "tab_general",
					text: "Général",
					link: "/services/optimus-avocats/dossiers/general.js",
					default: true,
					position: 100
				},
				{
					id: "tab_fichiers",
					text: "Fichiers",
					link: "/services/optimus-avocats/dossiers/fichiers.js",
					position: 200,
				},
				{
					id: "tab_notes",
					text: "Notes",
					link: "/services/optimus-avocats/dossiers/notes.js",
					position: 300
				},
				{
					id: "tab_authorizations",
					text: "Autorisations",
					link: "/services/optimus-avocats/dossiers/authorizations.js",
					position: 400
				},
				{
					id: "tab_intervenants",
					text: "Intervenants",
					link: "/services/optimus-avocats/dossiers/intervenants.js",
					position: 500,
				},
				{
					id: "tab_diligences",
					text: "Diligences",
					link: "/services/optimus-avocats/dossiers/diligences.js",
					position: 600,
				},
				{
					id: "tab_agenda",
					text: "Agenda",
					link: "/services/optimus-avocats/dossiers/agenda.js",
					position: 800,
				},
			]

		let owner_services = (this.owner != store.user.id) ? await rest(store.user.server + '/optimus-base/users/' + this.owner + '/services').then(response => response.data.map(service => service.name)) : store.services.map(service => service.name)
		store.services.map(service => 
		{
			if (owner_services.includes(service.name) && service.optimus_avocats_dossiers_tabs)
				tabs = tabs.concat(service.optimus_avocats_dossiers_tabs)
		})

		await load('/components/optimus_tabs.js', this.target,
			{
				router: true,
				tabs_container: document.getElementById('dossier_editor_tabs'),
				content_container: document.getElementById('dossier_editor_tabs_container'),
				tabs: tabs,
				params: response
			})

		document.querySelector('.delete-button').onclick = () => modal.open('/services/optimus-avocats/dossiers/delete_confirmation_modal.js')

		loader(this.target, false)
	}
}