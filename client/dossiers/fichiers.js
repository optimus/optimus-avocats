export default class OptimusAvocatsDossiersFichiers
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		let dossier = await rest(this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id)
		let owner = await rest(this.server + '/optimus-base/users/' + this.owner)
		console.log(owner)

		load('/components/optimus_webdav_explorer/index.js', this.target,
			{
				id: 'files',
				title: 'Fichiers',
				resource: 'files',
				server: 'https://' + store.user.server.replace('api.', 'cloud.'),
				root: 'files',
				path: owner.data.email + '/dossiers/' + dossier.data.displayname,
				cannotGetHigherThan: owner.data.email + '/dossiers/' + dossier.data.displayname,
				showBreadcrumb: false,
				showSearchBox: true,
				showRecursiveSearchButton: true,
			}
		)
	}
}