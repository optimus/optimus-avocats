export default class OptimusAvocatsPartages
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
		return this
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		//loader(this.target)

		await load('/services/optimus-avocats/dossiers/partages.html', this.target)

		await load('/components/optimus_authorizations/index.js', this.target,
			{
				server: store.user.server,
				url: 'https://{server}/optimus-base/authorizations',

				showTitle: false,
				title: null,
				showSubtitle: false,
				subtitle: null,
				showAddButton: (store.user.admin == false && store.user.id != this.owner) ? false : 'bottom',
				pagination: false,

				striped: true,
				bordered: true,
				columnSeparator: true,

				columns: ['user', 'read', 'write', 'delete'],
				headerFilterInputs: null,

				filters:
				{
					owner: this.owner,
					resource: 'dossiers/' + this.id
				},

				editor:
				{
					blocks: ['user', 'rights'],
					read: true,
					write: false,
					delete: false
				}
			})


		loader(this.target, false)
	}
}