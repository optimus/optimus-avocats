export default class OptimusAvocatsDossiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		await load('/services/optimus-avocats/dossiers/general.html', this.target, null, true)
		loader(document.getElementById('dossier_editor_container'), true)
		let response = await rest(this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id)

		await populate(this.target.querySelector('select[data-field="domaine"]'), store.user.server + '/optimus-avocats/constants/dossiers_domaines', true, true)
		await populate(this.target.querySelector('select[data-field="sous_domaine"]'), store.user.server + '/optimus-avocats/constants/dossiers_sousdomaines', true, true)
		let component = await load('/components/optimus_form.js', main, response.data)
		await populate(this.target.querySelector('select[data-field="sous_domaine"]'), store.user.server + '/optimus-avocats/constants/dossiers_sousdomaines', true, true, this.target.querySelector('select[data-field="sous_domaine"]').value, this.target.querySelector('select[data-field="domaine"]').value)

		this.target.querySelectorAll('.box').forEach(object => loader(object, false))

		this.target.querySelector('input[data-field="nom"]').select()

		this.target.querySelector('select[data-field="domaine"]').preventDefaultOnchange = true
		this.target.querySelector('select[data-field="domaine"]').addEventListener('change', async () =>
		{
			await component.save(this.target.querySelector('select[data-field="domaine"]'))
			store.history.pop()

			await populate(this.target.querySelector('select[data-field="sous_domaine"]'), store.user.server + '/optimus-avocats/constants/dossiers_sousdomaines', true, true, undefined, this.target.querySelector('select[data-field="domaine"]').value)
			if (this.target.querySelector('select[data-field="sous_domaine"]').value != this.target.querySelector('select[data-field="sous_domaine"]').lastSavedValue)
			{
				await component.save(this.target.querySelector('select[data-field="sous_domaine"]'))
				store.history.pop()
				store.history.push(async () =>
				{
					await component.undo(this.target.querySelector('select[data-field="domaine"]'))
					await populate(this.target.querySelector('select[data-field="sous_domaine"]'), store.user.server + '/optimus-avocats/constants/dossiers_sousdomaines', true, true, undefined, this.target.querySelector('select[data-field="domaine"]').value)
					await component.undo(this.target.querySelector('select[data-field="sous_domaine"]'))
				})
			}
			else
				store.history.push(async () => component.undo(this.target.querySelector('select[data-field="domaine"]')))
		})

		loader(document.getElementById('dossier_editor_container'), false)
	}
}