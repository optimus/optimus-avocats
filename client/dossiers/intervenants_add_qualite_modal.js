export default class OptimusAvocatsDossierIntervenantsAddQualiteModal
{
	constructor(target, params) 
	{
		this.target = target
		this.intervenant = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		loader(this.target, true)

		await load('/services/optimus-avocats/dossiers/intervenants_add_qualite_modal.html', this.target)

		let all_qualites = await populate(null, store.user.server + '/optimus-avocats/constants/intervenants_qualites')
		this.intervenant.qualites = await rest('https://' + this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants/' + this.intervenant.id).then(response => response.data.qualites)

		all_qualites.forEach(qualite =>
		{
			let template = document.getElementById('qualite-row').content.cloneNode(true).querySelector('div')
			template.querySelector('input').id = 'qualite_' + qualite.id
			template.querySelector('label').htmlFor = 'qualite_' + qualite.id
			template.querySelector('label').innerText = qualite.value

			if (this.intervenant.qualites.includes(qualite.id))
				template.querySelector('input').checked = true

			template.querySelector('label').onclick = () => 
			{
				if (template.querySelector('input').checked == true)
					rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants/' + this.intervenant.id + '/qualites/' + qualite.id, 'DELETE', null, 'toast')
						.then(response => response.code == 200 && this.intervenant.remove_qualite(qualite.id))
				else
					rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants/' + this.intervenant.id + '/qualites/' + qualite.id, 'POST', null, 'toast')
						.then(response => response.code == 201 && this.intervenant.add_qualite(qualite.id))
			}

			modal.querySelector('.qualites-container').appendChild(template)
		})

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.quit-button').onclick = () => modal.close()

		loader(this.target, false)
	}
}