export default class fileEvents
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{

		loader(this.target, true)

		const fileId = this.params.data.id.toString()

		await load('/components/optimus_calendar/index.js', this.target)
			.then((component) =>
			{

				let optimusCalendar = component.fullcalendar

				optimusCalendar.changeView('listYear')

				//document.querySelector('#calendar-view').classList.add('is-hidden')
				//document.querySelector('#calendar-actions').classList.add('is-hidden')
				document.querySelector('.optimus-calendar-search-button.button').classList.add('is-hidden')
				//document.querySelector('.optimus-calendar-today-button.button').classList.add('is-hidden')
				document.querySelector('.optimus-calendar-agendas-button.button').classList.add('is-hidden')
				document.querySelector('.optimus-calendar-weekends-button.button').classList.add('is-hidden')
				document.querySelector('#calendar-actions-mobile').classList.add('is-hidden')
				document.querySelector('#calendar-go-today').classList.add('is-hidden')
				// document.getElementById('optimus-calendar-search-bar').classList.remove('is-hidden')


				optimusCalendar.addEventSource(function (info, successCallback, failureCallback)
				{
					const calendarPromises = new Array()

					rest('https://' + store.user.server + '/optimus-calendar/' + store.user.id + '/calendars').then(calendars =>
					{
						calendars = calendars.data

						let endpoint

						const createEndpoints = async () =>
						{

							for (const calendar of calendars)
							{
								if (calendar.owner)
								{
									const auth = await rest(`${calendar.owner.server}/optimus-base/authorizations`, 'GET', { user: store.user.id, owner: calendar.owner.id, resource: 'calendars/' + calendar.owner.calendar }).then((data) =>
									{
										const auth = data.data[0]
										return auth
									})

									if (auth && auth.read === '0')
									{
										endpoint = null
									} else if (auth)
									{
										endpoint = `${calendar.owner.server}/optimus-calendar/${calendar.owner.id}/calendars/${calendar.owner.calendar}`
									}

								} else
								{
									endpoint = `${store.user.server}/optimus-calendar/${store.user.id}/calendars/${calendar.id}`
								}

								if (calendar.display === true && endpoint !== null)
								{
									let newPromise = rest(endpoint + '/events', 'GET', { property_search: { property: 'optimus_avocats_dossier_id', value: store.queryParams.id } }).then((data) =>
									{
										if (data && data.data)
										{
											data.data.map((el) => (el.calColor = calendar.color))
											data.data.map((el) => (el.calName = calendar.name))
											if (calendar.owner)
											{
												data.data.map((el) => (el.owner = calendar.owner.id))
												data.data.map((el) => (el.server = calendar.owner.server))
											} else
											{
												data.data.map((el) => (el.owner = store.user.id))
												data.data.map((el) => (el.server = store.user.server))
											}
											return data
										}
									})
									calendarPromises.push(newPromise)

								}
							}
						}

						createEndpoints().then(() =>
						{
							Promise.all(calendarPromises).then((values) =>
							{
								let myEvents = new Array()
								for (const el of values)
								{
									if (el && el.data)
									{
										for (const event of el.data)
										{
											event.backgroundColor = '#' + event.calColor
											event.borderColor = '#' + event.calColor
											event.allDay = event.allday
											delete event.allday

											if (event.allDay)
											{
												event.start = event.start + 'T00:00:00'
											}

											if (event.rrule)
											{
												const rule = rrule.RRule.fromString(event.rrule)

												if (rule.origOptions.byweekday)
												{
													const mappedWeek = rule.origOptions.byweekday.map((element) =>
													{
														return element.weekday
													})
													delete rule.origOptions.byweekday
													rule.origOptions.byweekday = mappedWeek
												}

												event.rrule = rule.origOptions
												const dT = new Date(event.start).toISOString().slice(0, 19) + 'Z'
												event.rrule.dtstart = dT

												delete event.end
											} else
											{
												event.duration = null
											}

											if (event.exdate)
											{
												event.exdate = event.exdate.slice(7).split(',')
											}

											myEvents.push(event)
										}
									}
								}
								successCallback(myEvents)
							})

						})
							.catch((err) =>
							{
								failureCallback(err)
							})
					})
				})

				optimusCalendar.on('eventClick', function (info)
				{

					modal.open('/services/optimus-calendar/calendars/editEvent.js', false, [info, optimusCalendar])
				})

				let didSelect = false

				optimusCalendar.on('dateClick', function (info)
				{
					info.title = document.querySelector('.title').innerText
					info.event =
					{
						extendedProps:
						{
							properties:
							{
								optimus_avocats_dossier_id: store.queryParams.id
							}
						}
					}
					if (!didSelect)
						modal.open('/services/optimus-calendar/calendars/newEvent.js', false, [info, optimusCalendar])
					didSelect = false
				})

				optimusCalendar.on('select', function (selectionInfo)
				{
					didSelect = true
					selectionInfo.title = document.querySelector('.title').innerText
					selectionInfo.event =
					{
						extendedProps:
						{
							properties:
							{
								optimus_avocats_dossier_id: store.queryParams.id
							}
						}
					}
					modal.open('/services/optimus-calendar/calendars/newEventBySelection.js', false, [selectionInfo, optimusCalendar])
				})

				document.querySelectorAll('.optimus-calendar-addevent-button').forEach((item) =>
				{
					item.addEventListener('click', () =>
					{
						const info = new Object()
						info.title = document.querySelector('.title').innerText
						info.event =
						{
							extendedProps:
							{
								properties:
								{
									optimus_avocats_dossier_id: store.queryParams.id
								}
							}
						}
						info.date = new Date()
						info.date.setMinutes(info.date.getMinutes() + 30)
						info.date.setMinutes(0)
						info.date.setSeconds(0)
						info.dateStr = info.date.toISOString().slice(0, 16)
						info.allDay = false
						modal.open('/services/optimus-calendar/calendars/newEvent.js', false, [info, optimusCalendar])
					})
				})
			})
			.then(() => loader(this.target, false)
			)

	}
}