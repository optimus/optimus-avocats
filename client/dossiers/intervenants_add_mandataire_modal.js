export default class OptimusAvocatsDossierIntervenantsAddMandataireModal
{
	constructor(target, params) 
	{
		this.target = target
		this.intervenant = params
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		console.log(this.component)
		await load('/services/optimus-avocats/dossiers/intervenants_add_mandataire_modal.html', this.target)
		await load('/components/optimus_form.js', modal.querySelector('.mandataire-block'))
		await populate(modal.querySelector('.type'), store.user.server + '/optimus-avocats/constants/intervenants_mandataires_types', true, false, 10)
		setTimeout(() => modal.querySelector('.mandataire').focus(), 10)

		modal.querySelector('.user-edit.button').onclick = () => 
		{
			if (modal.querySelector('.mandataire').dataset.id == 'null')
				return false
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + modal.querySelector('.mandataire').dataset.id + '#general')
		}

		modal.querySelector('.user-create.button').onclick = () =>
			rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.mandataire').value = response.data.displayname
						modal.querySelector('.mandataire').dataset.new_id = response.data.id
						modal.hide()
						router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + modal.querySelector('.mandataire').dataset.new_id + '#general')
					}
				})

		modal.onshow = () => setTimeout(() =>
			rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + (modal.querySelector('.mandataire').dataset.new_id || modal.querySelector('.mandataire').dataset.id))
				.then(response => 
				{
					modal.querySelector('.mandataire').value = response.data.displayname
					modal.querySelector('.mandataire').dataset.id = modal.querySelector('.mandataire').dataset.new_id
					modal.querySelector('.mandataire').classList.remove('is-danger')
				}), 50)

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				contact: modal.querySelector('.mandataire').dataset.id,
				type: modal.querySelector('.type').value,
			}
			rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants/' + this.intervenant.id + '/mandataires', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.close()
						this.intervenant.add_mandataire(response.data, true)
					}
				})
		}

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}