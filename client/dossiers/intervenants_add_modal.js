export default class OptimusAvocatsDossierIntervenantsAddIntervenantModal
{
	constructor(target, params) 
	{
		this.target = target
		this.component = params
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-avocats/dossiers/intervenants_add_modal.html', this.target)
		await load('/components/optimus_form.js', modal.querySelector('.intervenant-block'))
		setTimeout(() => modal.querySelector('.intervenant').focus(), 10)

		let intervenants_qualites = await populate(null, store.user.server + '/optimus-avocats/constants/intervenants_qualites')

		intervenants_qualites.forEach(qualite =>
		{
			let template = document.getElementById('qualite-row').content.cloneNode(true).querySelector('div')
			template.querySelector('input').id = 'qualite_' + qualite.id
			template.querySelector('input').dataset.id = qualite.id
			template.querySelector('label').htmlFor = 'qualite_' + qualite.id
			template.querySelector('label').innerText = intervenants_qualites.find(qualites => qualites.id == qualite.id)?.value

			modal.querySelector('.qualites-container').appendChild(template)
		})

		modal.querySelector('.user-edit.button').onclick = () => 
		{
			if (modal.querySelector('.intervenant').dataset.id == 'null')
				return false
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + modal.querySelector('.intervenant').dataset.id + '#general')
		}

		modal.querySelector('.user-create.button').onclick = () =>
			rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.intervenant').value = response.data.displayname
						modal.querySelector('.intervenant').dataset.new_id = response.data.id
						modal.hide()
						router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + modal.querySelector('.intervenant').dataset.new_id + '#general')
					}
				})

		modal.onshow = () => setTimeout(() =>
			rest('https://' + this.server + '/optimus-contacts/' + this.owner + '/contacts/' + (modal.querySelector('.intervenant').dataset.new_id || modal.querySelector('.intervenant').dataset.id))
				.then(response => 
				{
					modal.querySelector('.intervenant').value = response.data.displayname
					modal.querySelector('.intervenant').dataset.id = modal.querySelector('.intervenant').dataset.new_id
					modal.querySelector('.intervenant').classList.remove('is-danger')
				}), 50)

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				contact: modal.querySelector('.intervenant').dataset.id,
				qualites: Array.from(modal.querySelectorAll("input[type='checkbox']:checked"), ({ dataset }) => dataset.id),
				mandataires: []
			}

			rest('https://' + store.user.server + '/optimus-avocats/' + this.component.owner + '/dossiers/' + this.component.id + '/intervenants', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.close()
						this.component.add_intervenant(response.data, true)
					}
				})
		}

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}