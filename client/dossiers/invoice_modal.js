export default class OptimusAvocatsDossiersInvoiceModal
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-avocats/dossiers/invoice_modal.html', this.target)

		this.dossier = await rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id)
		await this.populate_businesses()
		this.populate_clients()
		await populate(this.target.querySelector('.vat_rate'), store.user.server + '/optimus-business/constants/vat_rates', false, false, 60)
		if (modal.querySelector('.business').length == 1)
			modal.querySelector('.business-block').classList.add('is-hidden')

		modal.querySelector('.business').onchange = async () => 
		{
			this.entreprise = await rest('https://api.' + modal.querySelector('.business').value.split('#')[0] + '/optimus-businesses/businesses/' + modal.querySelector('.business').value.split('#')[1], 'GET')
			if (this.entreprise.data.vat_scheme == 3)
				modal.querySelector('.vat_rate').value = 5
			this.populate_templates(modal.querySelector('.business').value.split('#')[1])
		}
		modal.querySelector('.business').onchange()

		modal.querySelector('.number-suffix').onchange = event => event.target.value = parseInt(event.target.value).toString().padStart(5, '0')

		modal.querySelector('.client').onchange = async () =>
		{
			rest('https://' + store.user.server + '/optimus-contacts/' + this.owner + '/contacts/' + modal.querySelector('.client').value)
				.then(response =>
				{
					let ue_countries = [15, 22, 34, 55, 57, 58, 59, 68, 73, 74, 81, 84, 98, 104, 106, 118, 124, 125, 133, 151, 171, 172, 176, 193, 194, 199, 205]
					if (this.entreprise.data.vat_scheme == 3)
						modal.querySelector('.vat_rate').value = 50
					else if (!ue_countries.includes(response.data.country))
						modal.querySelector('.vat_rate').value = 40
					else if (response.data.tva)
						modal.querySelector('.vat_rate').value = 30
					else
						modal.querySelector('.vat_rate').value = 60
				})
		}

		modal.querySelector('.client-edit.button').onclick = () => 
		{
			modal.hide()
			router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + modal.querySelector('.client').value + '#general')
		}

		modal.querySelector('.create-button').onclick = () =>
		{
			let data = {
				"number": parseInt(modal.querySelector('.number-suffix').value),
				"vat_rate": modal.querySelector('.vat_rate').value,
				"template": modal.querySelector('.template').value,
				"client_endpoint": "/optimus-contacts/{owner}/contacts/{id}",
				"client_owner": this.owner,
				"client_id": modal.querySelector('.client').value,
				"details_endpoint": "/optimus-avocats/{owner}/dossiers/{reference}/diligences",
				"details_owner": this.owner,
				"details_ids": this.params.diligences_ids,
				"reference_endpoint": "/optimus-avocats/{owner}/dossiers/{id}",
				"reference_owner": this.owner,
				"reference_id": this.dossier.data.id,
			}
			rest('api.' + modal.querySelector('.business').value.split('#')[0] + '/optimus-business/' + modal.querySelector('.business').value.split('#')[1] + '/invoices', 'POST', data)
				.then(response => 
				{
					if (response.code == 418)
					{
						modal.querySelector('.number-prefix').value = new Date().getFullYear().toString().slice(2, 4) + '-'
						modal.querySelector('.number-suffix').value = '00001'
						modal.querySelector('.number-block').classList.remove('is-hidden')
						modal.querySelector('.number-suffix').setCustomValidity('Renseignez le numéro ici')
						modal.querySelector('.number-suffix').reportValidity()
						modal.querySelector('.number-suffix').focus()
						modal.querySelector('.number-suffix').onkeyup = () =>
						{
							modal.querySelector('.number-suffix').setCustomValidity()
							modal.querySelector('.number-suffix').reportValidity()
						}
					}
					else if (response.code == 201)
					{
						router('optimus-avocats/dossiers/editor?owner=' + this.owner + '&id=' + this.id + '#diligences')
						modal.close()
					}
				})
		}

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}

	async populate_businesses()
	{
		clear(modal.querySelector('.business'))
		return rest(store.user.server + '/optimus-business/users/' + store.user.id + '/businesses', 'GET')
			.then(businesses => 
			{
				businesses.data.sort((a, b) => a.id < b.id ? 1 : -1)
				for (let business of businesses.data)
					modal.querySelector('.business').add(new Option(business.displayname, business.server + '#' + business.id))
			})
	}

	async populate_clients()
	{
		clear(modal.querySelector('.client'))
		return rest(store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants')
			.then(clients => 
			{
				clients.data.sort((a, b) => a.displayname > b.displayname ? 1 : -1)
				for (let client of clients.data)
					if (client.qualites.includes(10))
						modal.querySelector('.client').add(new Option(client.displayname, client.contact))
				if (modal.querySelector('.client').options.length == 0)
					rest(store.user.server + '/optimus-contacts/' + this.owner + '/contacts')
						.then(contacts => 
						{
							contacts.data.sort((a, b) => a.displayname > b.displayname ? 1 : -1)
							for (let contact of contacts.data)
								modal.querySelector('.client').add(new Option(contact.displayname, contact.id))
						})
			})

		return
	}

	async populate_templates(business_id)
	{
		clear(modal.querySelector('.template'))
		let business = await rest(store.user.server + '/optimus-base/users/' + business_id, 'GET')

		return fetch('https://' + store.user.server.replace('api', 'cloud') + '/files/' + business.data.email + '/modeles/factures',
			{
				method: 'PROPFIND',
				credentials: 'include'
			})
			.then(response => response.text())
			.then(response => new DOMParser().parseFromString(response, 'text/xml'))
			.then(nodes => nodes.getElementsByTagName('d:response'))
			.then(nodes =>
			{
				let files = []
				for (let node of nodes)
					if (node.getElementsByTagName('d:collection').length == 0)
						files.push(
							{
								path: decodeURIComponent(node.getElementsByTagName('d:href')[0].childNodes[0].nodeValue.slice(6)),
								displayname: decodeURIComponent(node.getElementsByTagName('d:href')[0].childNodes[0].nodeValue.split('/').at(-1)),
								lastmodified: node.getElementsByTagName('d:getlastmodified')[0].childNodes[0].nodeValue,
							}
						)
				files.sort((a, b) => a.lastmodified < b.lastmodified ? 1 : -1)
				files.forEach(file => modal.querySelector('.template').add(new Option(file.displayname, file.displayname)))
			})
	}
}