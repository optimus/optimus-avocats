export default class OptimusAvocatsDossiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		loader(this.target, true)
		await load('/services/optimus-avocats/dossiers/intervenants.html', this.target)

		this.intervenants_qualites = await populate(null, store.user.server + '/optimus-avocats/constants/intervenants_qualites')
		this.intervenants_mandataires_types = await populate(null, store.user.server + '/optimus-avocats/constants/intervenants_mandataires_types')

		rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants')
			.then(response => response.data.forEach(intervenant => this.add_intervenant(intervenant)))

		main.querySelector('.intervenant-add-button').onclick = () => modal.open('/services/optimus-avocats/dossiers/intervenants_add_modal.js', false, this)

		loader(this.target, false)
	}

	async add_intervenant(data, animation)
	{
		let intervenant = new Object
		intervenant.id = data.id
		intervenant.DOMObject = document.getElementById('intervenant').content.cloneNode(true).querySelector('article')
		intervenant.DOMObject.querySelector('.intervenant-displayname').innerHTML = data.displayname

		intervenant.add_qualite = id =>
		{
			intervenant.DOMObject.querySelector('.qualites-block').classList.remove('is-hidden')
			let qualite_template = document.getElementById('qualite').content.cloneNode(true).querySelector('span')
			qualite_template.dataset.id = id
			qualite_template.querySelector('.qualite-text').innerText = this.intervenants_qualites.find(qualite => qualite.id == id)?.value
			qualite_template.querySelector('.qualite-delete-button').onclick = event => rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants/' + data.id + '/qualites/' + id, 'DELETE', null, 'toast')
				.then(response => response.code == 200 && intervenant.remove_qualite(event.target.parentNode.dataset.id))
			intervenant.DOMObject.querySelector('.qualites-container').appendChild(qualite_template)
		}

		intervenant.remove_qualite = id =>
		{
			intervenant.DOMObject.querySelector('.qualites-container').querySelector("[data-id='" + id + "']").remove()
			if (intervenant.DOMObject.querySelector('.qualites-container').childNodes.length == 0)
				intervenant.DOMObject.querySelector('.qualites-block').classList.add('is-hidden')
		}

		intervenant.add_mandataire = mandataire =>
		{
			let mandataire_template = document.getElementById('mandataire').content.cloneNode(true).querySelector('div')
			mandataire_template.querySelector('.mandataire-type').innerHTML = this.intervenants_mandataires_types.find(type => type.id == mandataire.type)?.value
			mandataire_template.querySelector('.mandataire-input').value = mandataire.displayname
			mandataire_template.querySelector('.mandataire-edit-button').onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + mandataire.contact + '#general')
			mandataire_template.querySelector('.mandataire-delete-button').onclick = event => rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants/' + data.id + '/mandataires/' + mandataire.id, 'DELETE', null, 'toast')
				.then(response => response.code == 200 && intervenant.remove_mandataire(event.target))
			intervenant.DOMObject.querySelector('.mandataires-container').appendChild(mandataire_template)
		}

		intervenant.remove_mandataire = obj =>
		{
			obj.parentNode.parentNode.parentNode.parentNode.parentNode.remove()
		}

		intervenant.DOMObject.querySelector('.message-header').onclick = function ()
		{
			this.parentNode.querySelector('.message-body').classList.toggle('is-hidden')
			this.querySelector('.intervenant-expand-button').classList.toggle('fa-caret-down')
			this.querySelector('.intervenant-expand-button').classList.toggle('fa-caret-right')
		}

		intervenant.DOMObject.querySelector('.intervenant-edit-button').onclick = event => 
		{
			router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + data.contact + '#general')
			event.stopPropagation()
		}

		intervenant.DOMObject.querySelector('.intervenant-add-qualite-button').onclick = () => modal.open('/services/optimus-avocats/dossiers/intervenants_add_qualite_modal.js', false, intervenant)

		intervenant.DOMObject.querySelector('.intervenant-add-mandataire-button').onclick = () => modal.open('/services/optimus-avocats/dossiers/intervenants_add_mandataire_modal.js', false, intervenant)

		intervenant.DOMObject.querySelector('.intervenant-delete-button').onclick = event => rest('https://' + store.user.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/intervenants/' + data.id, 'DELETE', null, 'toast')
			.then(response => 
			{
				if (response.code == 200)
				{
					event.target.parentNode.parentNode.classList.add('animate__animated', 'animate__zoomOut', 'animate__faster')
					event.target.parentNode.parentNode.addEventListener('animationend', () => event.target.parentNode.parentNode.remove())
				}
			})

		data.qualites.forEach(qualite => intervenant.add_qualite(qualite))
		data.mandataires.forEach(mandataire => intervenant.add_mandataire(mandataire))

		if (animation)
			intervenant.DOMObject.classList.add('animate__animated', 'animate__zoomIn', 'animate__faster')
		document.getElementById('intervenants').appendChild(intervenant.DOMObject)
	}
}