load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
load_script('/libs/luxon.min.js')
import { Tabulator, AjaxModule, ColumnCalcsModule, EditModule, ExportModule, FilterModule, FormatModule, GroupRowsModule, InteractionModule, KeybindingsModule, MutatorModule, SelectRowModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([AjaxModule, ColumnCalcsModule, EditModule, ExportModule, FilterModule, FormatModule, GroupRowsModule, InteractionModule, KeybindingsModule, MutatorModule, SelectRowModule])

export default class OptimusAvocatsDossiersDiligences
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
		return this
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		this.users = await rest(this.server + '/optimus-base/users')

		await load('/services/optimus-avocats/dossiers/diligences.html', this.target)
		loader(this.target)
		let datepicker = await load('/components/optimus_table/datepicker.js')

		let intervenants = await rest('https://' + this.server + '/optimus-base/users').then(response => response.data.map(item => { return { label: item.displayname, value: item.id } }))
		let categories = await rest('https://' + this.server + '/optimus-avocats/constants/diligences_categories').then(response => response.data.map(item => { return { label: item.value, value: item.id } }))
		let subcategories = await rest('https://' + this.server + '/optimus-avocats/constants/diligences_subcategories').then(response => response.data.map(item => { return { label: item.value, value: item.id, category: item.category } }))

		this.target.querySelector('.optimus-table').classList.add('is-striped', 'is-bordered', 'is-bordered-vertical')

		this.tabulator = new Tabulator(this.target.querySelector('.optimus-table-container'),
			{
				ajaxURL: this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/diligences',
				ajaxRequestFunc: (url, ajaxConfig, params) => new Promise(function (resolve, reject)
				{
					params.sort =
						[
							{ field: 'invoice_number', dir: 'desc' },
							{ field: 'date', dir: 'asc' }
						]
					if (!store.user.server)
						modal.open('/services/optimus-client/login/index.js')
					else
						rest(url, ajaxConfig.method, params, 'toast')
							.then(response =>
							{
								if (response.code >= 400)
									reject(response.message)
								else
									resolve(response.data)
							})
				}),
				ajaxConfig: {
					method: 'GET',
					credentials: 'include'
				},
				columns:
					[
						{
							titleFormatter: "rowSelection",
							width: 42,
							hozAlign: "center",
							vertAlign: 'middle',
							formatter: cell => cell.getData().invoice_id ? null : '<input type="checkbox" aria-label="Select Row">',
							cellClick: (e, cell) => cell.getRow().toggleSelect(),
						},
						{
							field: 'date',
							title: 'Date',
							width: 120,
							hozAlign: 'center',
							vertAlign: 'middle',
							formatter: "datetime",
							formatterParams:
							{
								inputFormat: "yyyy-MM-dd",
								outputFormat: "dd/MM/yyyy",
								invalidPlaceholder: '<span class="has-text-danger">date invalide</span>',
							},
							editable: cell => !cell.getData().invoice_id,
							editor: datepicker.open,
							editorParams:
							{
								optimusDatepicker:
								{
									type: 'date',
									required: true
								}
							}
						},
						{
							field: 'intervenant',
							title: 'Intervenant',
							width: 180,
							vertAlign: 'middle',
							visible: this.users.data.length > 1,
							formatter: cell => intervenants?.filter(item => item.value == cell.getValue())[0]?.label,
							editor: 'list',
							editorParams:
							{
								allowEmpty: false,
								values: intervenants,
								itemFormatter: (label, value, item, element) => label
							}
						},
						{
							field: 'description',
							title: 'Description',
							width: 670,
							formatter: 'textarea',
							editable: cell => !cell.getData().invoice_id,
							editor: 'textarea',
							editorParams:
							{
								selectContents: true,
								verticalNavigation: 'hybrid'
							},
							cellEdited: cell => cell.getTable().redraw()
						},
						{
							field: 'category',
							title: 'Catégorie',
							width: 117,
							vertAlign: 'middle',
							formatter: cell => categories?.filter(item => item.value == cell.getValue())[0]?.label,
							editable: cell => !cell.getData().invoice_id,
							editor: 'list',
							editorParams:
							{
								allowEmpty: false,
								values: categories,
								itemFormatter: (label, value, item, element) => label,
								verticalNavigation: 'hybrid'
							}
						},
						{
							field: 'type',
							title: 'Type',
							width: 170,
							vertAlign: 'middle',
							formatter: cell => subcategories?.filter(item => item.value == cell.getValue())[0]?.label,
							editable: cell => !cell.getData().invoice_id,
							editor: 'list',
							editorParams:
							{
								allowEmpty: false,
								valuesLookup: (cell, filterTerm) => subcategories.filter(item => item.category == cell.getData().category),
								itemFormatter: (label, value, item, element) => label,
								verticalNavigation: 'hybrid'
							}
						},
						{
							field: 'quantity',
							title: 'Quantité',
							width: 90,
							hozAlign: 'right',
							vertAlign: 'middle',
							formatter: 'money',
							formatterParams: { decimal: ",", thousand: " ", symbol: null },
							editable: cell => !cell.getData().invoice_id,
							editor: 'number',
							editorParams:
							{
								selectContents: true,
								min: 0,
								max: 99999999.99,
								step: 0.01,
								verticalNavigation: 'table'
							},
						},
						{
							field: 'price',
							title: 'Prix',
							width: 110,
							hozAlign: 'right',
							vertAlign: 'middle',
							formatter: 'money',
							formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true },
							editable: cell => !cell.getData().invoice_id,
							editor: 'number',
							editorParams:
							{
								selectContents: true,
								min: 0,
								max: 99999999.99,
								step: 0.01,
								verticalNavigation: 'table'
							},

						},
						{
							field: 'total',
							title: 'Total',
							width: 110,
							hozAlign: 'right',
							vertAlign: 'middle',
							formatter: "money",
							formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true },
							mutator: function (value, data, type, mutatorParams) { return data.quantity * data.price },
							bottomCalc: 'sum',
							bottomCalcFormatter: 'money',
							bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 }
						},
					],
				groupBy: function (data)
				{
					if (!data.invoice_id)
						return 'Non facturé'
					else
						return 'Facture N°' + data.invoice_number + ' (' + data.invoice_business_displayname + ')'
				},
				groupHeader: (value, count, data, group) =>
				{
					if (!data[0].invoice_number)
						return value

					let title = document.createElement('span')
					title.classList.add('has-text-white', 'p-0', 'm-0')
					title.innerText = value

					let invoice_link = document.createElement('i')
					invoice_link.classList.add('fas', 'fa-pen', 'ml-4')
					invoice_link.onclick = event => 
					{
						router('optimus-business/invoices/editor?owner=' + data[0].invoice_owner + '&id=' + data[0].invoice_id + '#general')
						event.stopPropagation()
					}

					let invoice_odt = document.createElement('i')
					invoice_odt.classList.add('fas', 'fa-file', 'ml-4')
					invoice_odt.onclick = event => 
					{
						event.target.classList.replace('fa-file', 'fa-spinner')
						event.target.classList.add('fa-spin')
						rest('https://api.' + data[0].invoice_server + '/optimus-business/' + data[0].invoice_owner + '/invoices/' + data[0].invoice_id + '/odt')
							.then(file => 
							{
								if (file.code == 200)
								{
									var element = document.createElement('a')
									element.href = 'data:application/vnd.oasis.opendocument.text;base64,' + file.data
									element.download = data[0].invoice_number + '.odt'
									element.click()
								}
								event.target.classList.replace('fa-spinner', 'fa-file')
								event.target.classList.remove('fa-spin')
							})
						event.stopPropagation()
					}

					let invoice_pdf = document.createElement('i')
					invoice_pdf.classList.add('fas', 'fa-file-pdf', 'ml-4')
					invoice_pdf.onclick = event => 
					{
						event.target.classList.replace('fa-file-pdf', 'fa-spinner')
						event.target.classList.add('fa-spin')
						rest('https://api.' + data[0].invoice_server + '/optimus-business/' + data[0].invoice_owner + '/invoices/' + data[0].invoice_id + '/pdf')
							.then(file => 
							{
								if (file.code == 200)
								{
									var element = document.createElement('a')
									element.href = 'data:application/pdf;base64,' + file.data
									element.download = data[0].invoice_number + '.pdf'
									element.click()
								}
								event.target.classList.replace('fa-spinner', 'fa-file-pdf')
								event.target.classList.remove('fa-spin')
							})

						event.stopPropagation()
					}

					let header = document.createElement('span')
					header.classList.add('p-0', 'm-0')
					header.appendChild(title)
					header.appendChild(invoice_link)
					header.appendChild(invoice_odt)
					header.appendChild(invoice_pdf)

					return header
				},
				groupStartOpen: value => value == 'Non facturé',
				groupToggleElement: "header",
				selectableCheck: row => !row.getData().invoice_id,
				dataLoader: false,
				ajaxContentType: 'json',
				layout: 'fitData',
				layoutColumnsOnNewData: false,
				filterMode: "remote",
				locale: 'fr',
				placeholder: 'Aucun résultat',
				langs: {
					'fr': {
						ajax: {
							loading: "Chargement",
							error: "Erreur de chargement",
						},
					}
				},
			})

		this.tabulator.on("dataLoadError", error => this.clearData())

		this.tabulator.on("rowSelected", row =>
		{
			optimusToast(this.recapToast(), 'is-link', { single: true })
			if (row.getData().invoice_id)
				row.deselect()
			if (this.tabulator.getSelectedRows().length == 1)
				this.toggleCopyPasteButtons()
			return row.getElement().querySelector('input[type=checkbox]') ? row.getElement().querySelector('input[type=checkbox]').checked = true : false
		})

		this.tabulator.on("rowDeselected", row =>
		{
			optimusToast(this.recapToast(), 'is-link', { single: true })
			if (this.tabulator.getSelectedRows().length == 0)
				this.toggleCopyPasteButtons()
			return row.getElement().querySelector('input[type=checkbox]') ? row.getElement().querySelector('input[type=checkbox]').checked = false : false
		})

		this.tabulator.on("cellEdited", async cell =>
		{
			if (this.cancelEdit)
				return delete this.cancelEdit

			let field = cell.getField()
			if (field == 'total')
				return true
			else if (field == 'category')
				cell.getRow().getCell('type').setValue(subcategories.filter(item => item.category == cell.getValue())[0].value)

			rest(this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/diligences/' + cell.getData().id, 'PATCH', { [field]: cell.getValue() }, 'toast')
				.then(response =>
				{
					if (response.code != 200)
					{
						if (cell.getField() == 'date' || cell.getField() == 'intervenant' || cell.getField() == 'category' || cell.getField() == 'type' || cell.getField() == 'quantity' || cell.getField() == 'price')
							cell.restoreOldValue()
						else
							cell.getElement().style.color = '#FF0000'
					}
					else
					{
						cell.getElement().style.color = 'unset'
						if (field == 'quantity' || field == 'price')
						{
							cell.getRow().getCell('total').setValue(cell.getData('quantity') * cell.getData('price'), 2)
							let total = 0
							for (let row of this.tabulator.getData())
								if (!row.invoice_id)
									total += row.quantity * row.price
							this.target.querySelector('.tabulator-calcs-bottom:last-of-type > div:last-of-type').innerText = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(total)
							if (this.tabulator.getSelectedRows().length > 0)
								optimusToast(this.recapToast(), 'is-link', { single: true })
						}
					}
				})
		})

		this.target.querySelector('.add-diligence.button').onclick = () =>
		{
			rest(this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/diligences', 'POST', {}, 'toast')
				.then(response =>
				{
					if (response.code == 201)
					{
						this.tabulator.addData([response.data])
						this.tabulator.getRows()[this.tabulator.getRows().length - 1].getCell('description').getElement().focus()
						this.tabulator.selectRow(response.data.id)
						this.toggleCopyPasteButtons()
					}
				})
		}

		this.target.querySelector('.invoice.button').onclick = () =>
		{
			if (!store.services.find(service => service.name == 'optimus-business'))
				return optimusToast('Le service optimus-business doit être installé avant de pouvoir facturer', 'is-warning')
			if (this.tabulator.getSelectedRows().length == 0)
				return optimusToast('Veuillez sélectionner les diligences que vous souhaitez facturer', 'is-warning')
			this.tabulator.diligences_ids = this.tabulator.getSelectedRows().map(diligence => diligence.getData().id)
			modal.open('/services/optimus-avocats/dossiers/invoice_modal.js', false, this.tabulator)
		}

		return new Promise(resolve =>
		{
			this.tabulator.on('tableBuilt', () =>
			{
				let width = 0
				this.tabulator.columnManager.columns.map(column => width += column.visible ? column.width : 0)
				this.target.firstElementChild.style.setProperty('max-width', 3 + width + 'px', 'important')
				this.target.querySelector('.tabulator-footer').style.display = 'none'
				setTimeout(() => this.toggleCopyPasteButtons(), 200)
				loader(this.target, false)
				resolve(this)
			})
		})
	}

	toggleCopyPasteButtons()
	{
		if (!this.target.querySelector('.tabulator-calcs-bottom:last-of-type > div:nth-child(1)'))
			return false
		this.target.querySelector('.tabulator-calcs-bottom:last-of-type > div:nth-child(1)').style.padding = 0
		this.target.querySelector('.tabulator-calcs-bottom:last-of-type > div:nth-child(1)').innerHTML = '<i class="icon fas fa-turn-up p-0 m-0" style="transform:rotate(90deg)">'

		this.target.querySelector('.tabulator-calcs-bottom:last-of-type > div:nth-child(2)').style.padding = '0.2rem'
		this.target.querySelector('.tabulator-calcs-bottom:last-of-type > div:nth-child(2)').innerText = null
		this.target.querySelector('.tabulator-calcs-bottom:last-of-type > div:nth-child(2)').appendChild(this.target.querySelector('#copy-paste-buttons').content.cloneNode(true))

		this.target.querySelector('.delete-button').style.visibility = (this.tabulator.getSelectedData().length == 0) ? 'hidden' : ''
		this.target.querySelector('.cut-button').style.visibility = (this.tabulator.getSelectedData().length == 0) ? 'hidden' : ''
		this.target.querySelector('.copy-button').style.visibility = (this.tabulator.getSelectedData().length == 0) ? 'hidden' : ''
		this.target.querySelector('.paste-button').style.visibility = JSON.parse(localStorage.getItem('clipboard'))?.content == 'diligences_data' ? '' : 'hidden'

		//DELETE
		this.target.querySelector('.delete-button').onclick = () =>
		{
			for (let row of this.tabulator.getSelectedRows())
				rest(this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/diligences/' + row.getData().id, 'DELETE', {}, 'toast')
					.then(response =>
					{
						if (response.code == 200)
							row.delete()
						this.toggleCopyPasteButtons()
					})
		}

		//COPY
		this.target.querySelector('.copy-button').onclick = () =>
		{
			let clipboard = new Object
			clipboard.content = 'diligences_data'
			clipboard.data = this.tabulator.getSelectedData()
			localStorage.setItem('clipboard', JSON.stringify(clipboard))
			optimusToast('Les données ont été copiées dans le presse-papier', 'is-success')
			this.toggleCopyPasteButtons()
		}

		//CUT
		this.target.querySelector('.cut-button').onclick = () =>
		{
			this.target.querySelector('.copy-button').click()
			this.target.querySelector('.delete-button').click()
		}

		//PASTE
		this.target.querySelector('.paste-button').onclick = () =>
		{
			let clipboard = JSON.parse(localStorage.getItem('clipboard'))
			if (clipboard?.content != 'diligences_data')
				return optimusToast('Ces données ne peuvent pas être collées ici', 'is-danger')
			for (let row of clipboard.data)
			{
				let insert =
				{
					date: row.date,
					intervenant: row.intervenant,
					description: row.description,
					category: row.category,
					type: row.type,
					quantity: row.quantity,
					price: row.price
				}
				rest(this.server + '/optimus-avocats/' + this.owner + '/dossiers/' + this.id + '/diligences', 'POST', insert, 'toast')
					.then(response =>
					{
						if (response.code == 201)
							this.tabulator.addData([response.data])
						this.toggleCopyPasteButtons()
					})
			}
		}
	}

	recapToast()
	{
		let honoraires = 0
		let frais = 0
		let debours = 0

		for (let row of this.tabulator.getSelectedData())
		{
			if (row.category == 1)
				honoraires += row.quantity * row.price
			if (row.category == 2)
				frais += row.quantity * row.price
			if (row.category == 3)
				debours += row.quantity * row.price
		}

		let total_ht = honoraires + frais
		let tva = total_ht * 0.2
		let total_ttc = total_ht + debours + tva

		let table = main.querySelector('#recap-toast').content.firstElementChild.outerHTML
			.replace('{HONORAIRES}', new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(honoraires))
			.replace('{FRAIS}', new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(frais))
			.replace('{TOTAL_HT}', new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(total_ht))
			.replace('{TVA}', new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(tva))
			.replace('{DEBOURS}', new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(debours))
			.replace('{TOTAL_TTC}', new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(total_ttc))
		return table
	}

	onUnload()
	{
		this.tabulator.destroy()
		delete this.tabulator
		return this
	}

	onWindowResize()
	{

	}
}