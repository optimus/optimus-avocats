export default class OptimusAvocatsCalendarsEventModalDossierTab
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	async init()
	{
		await load('/services/optimus-avocats/calendars/event_modal_dossier_tab.html', document.getElementById('calendar_tab_dossier'))

		//if (this.params?.event?.extendedProps)
		//{
		//if (!this.params?.event?._def?.publicId)
		//modal.querySelector('.file-edit-button').classList.add('is-hidden')
		await this.init_file_finder(this.params.event?.extendedProps?.properties?.optimus_avocats_dossier_id)
		setTimeout(() => document.getElementById('calendar-select').addEventListener('change', () => this.init_file_finder()), 400)
		//}


		modal.querySelector('.file-edit-button').onclick = () =>
		{
			if (document.getElementById('file-finder').dataset.id && document.getElementById('file-finder').dataset.id != 'null')
			{
				modal.hide()
				router('optimus-avocats/dossiers/editor?owner=' + document.getElementById('file-finder').dataset.owner + '&id=' + document.getElementById('file-finder').dataset.id)
			}
		}


	}

	async init_file_finder(dossier_id)
	{
		setTimeout(() => 
		{
			document.getElementById('calendar-select').querySelectorAll('option').forEach(async option => 
			{
				if (document.getElementById('calendar-select').value == option.value)
				{
					if (!this.fileFinderOwner || this?.fileFinderOwner != option.dataset.owner)
					{
						if (this.fileFinderOwner && this.fileFinderOwner != option.dataset.owner && document.getElementById('file-finder').dataset.id != 'null')
						{
							delete document.getElementById('file-finder').dataset.id
							document.getElementById('file-finder').value = ''
							optimusToast("Attention : le changement d'agenda a supprimé le lien avec le dossier", "is-warning")
						}
						document.getElementById('file-finder').dataset.owner = option.dataset.owner
						if (dossier_id)
							document.getElementById('file-finder').dataset.id = dossier_id
						else
							delete document.getElementById('file-finder').dataset.id
						this.fileFinderOwner = option.dataset.owner
						if (!this.form)
							this.form = await load('/components/optimus_form.js', document.getElementById('calendar_tab_dossier'))
						document.getElementById('file-finder').resource_endpoint = store.user.server + store.resources.find(item => item.id == document.getElementById('file-finder').dataset.resource).endpoint?.replace('{owner}', option.dataset.owner)
					}
				}
			})
		}, 200)

	}
}