export default class OptimusAvocatsDossiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-avocats/jitsi/index.html', main)
		clear(main.querySelector('.jitsi-meeting-link'))

		const chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
		let random = ""
		for (let i = 0; i < 12; i++)
			random += chars.charAt(Math.floor(Math.random() * chars.length))

		let a = document.createElement('a')
		a.target = '_blank'
		a.href = 'https://meet.jit.si/' + random
		a.innerText = a.href
		main.querySelector('.jitsi-meeting-link').appendChild(a)

		main.querySelector('.jitsi-meeting-link-copy-button').onclick = () =>
		{
			navigator.clipboard.writeText(main.querySelector('.jitsi-meeting-link').innerText)
			optimusToast('Le lien a été copié dans le presse papier')
		}
	}
}