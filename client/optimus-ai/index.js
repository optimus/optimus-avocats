export default class OptimusAvocatsDossiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-avocats/optimus-ai/index.html', main)

		document.querySelector('.input.question').focus()

		document.querySelector('.gpt-chat').message = [
			{
				role: "user",
				content: "Tu es un assistant juridique destiné à aider les avocats français dans leur travail et tu réponds en appliquant le droit français.",
			},
		]

		//SEND TEXT MESSAGE
		main.querySelector('.input.question').onkeyup = event => event.key == 'Enter' && main.querySelector('.text-prompt-button').click()
		main.querySelector('.text-prompt-button').onclick = async () => 
		{
			document.querySelector('.gpt-chat').message.push
				(
					{
						role: "user",
						content: document.querySelector('.input.question').value,
					}
				)
			ask(undefined, main.querySelector('.input.question').value)
		}

		//IMAGE UPLOAD
		const imageInput = document.querySelector('.image-input')
		imageInput.onchange = () =>
		{
			if (imageInput.files.length > 0)
			{
				const fileReader = new FileReader()
				fileReader.readAsDataURL(imageInput.files[0])
				fileReader.onload = () =>
				{
					document.querySelector('.gpt-chat').message.push
						(
							{
								"role": "user",
								"content": [
									{
										"type": "image_url",
										"image_url":
										{
											"url": fileReader.result,
											"detail": "auto"
										}
									}
								]
							},
						)
					document.querySelector('.gpt-chat').message.push
						(
							{
								role: "assistant",
								content: "Que voulez-vous savoir à propos de cette image ?",
							}
						)

					const template = document.getElementById('gpt-block').content.firstElementChild.cloneNode(true)
					main.querySelector('.gpt-chat').appendChild(template)
					let img = document.createElement('img')
					img.src = fileReader.result
					template.querySelector('.block-question').appendChild(img)
					template.querySelector('.block-answer').innerText = "Que voulez-vous savoir à propos de cette image ?"

					main.querySelector('.input.question').value = ""
					main.querySelector('.input.question').focus()
					main.querySelector('.gpt-disclaimer').classList.add('is-hidden')
					main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
					main.querySelector('.input-block').classList.remove('is-hidden')

					setTimeout(() => document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight, 100)
				}
			}
		}

		//AUDIO UPLOAD
		const audioInput = document.querySelector('.audio-input')
		audioInput.onchange = () =>
		{
			if (audioInput.files.length > 0)
			{
				const fileReader = new FileReader()
				fileReader.readAsDataURL(audioInput.files[0])
				fileReader.onload = () => ask_whisper(audioInput.files[0])
			}
		}

		//PRESET SELECTOR
		main.querySelector('.preset-selector').onchange = () =>
		{
			if (main.querySelector('.preset-selector').value == 1)
			{
				document.querySelector('.gpt-chat').message.push
					(
						{
							role: "user",
							content: "Peux tu me fournir en réponse uniquement le code JSON pour un objet dont la structure est la suivante : un objet de type string nommé 'description' contenant un résumé de 64 caractères maximum précisant la nature de la dépense (par exemple : Restaurant suivi du nom du restaurant), un objet de type nombre decimal nommé 'ht' indiquant le montant hors taxes payé, un objet de type nombre decimal nommé 'tva' indiquant le montant de la tva payée, un objet nommé 'compte' contenant uniquement le numéro du compte comptable qui serait selon toi le plus indiqué pour comptabiliser cette dépense selon les normes françaises de comptabilité, un objet nommé 'compte_description' contenant la description du compte comptable précité, ainsi qu'un tableau nommé 'repartition' indiquant en majuscules les initiales portées manuscritement en haut du document ?",
						}
					)
				ask('gpt-4-turbo', "Peux tu me fournir en réponse uniquement le code JSON pour un objet dont la structure est la suivante : un objet de type string nommé 'description' contenant un résumé de 64 caractères maximum précisant la nature de la dépense (par exemple : Restaurant suivi du nom du restaurant), un objet de type nombre decimal nommé 'ht' indiquant le montant hors taxes payé, un objet de type nombre decimal nommé 'tva' indiquant le montant de la tva payée, un objet nommé 'compte' contenant uniquement le numéro du compte comptable qui serait selon toi le plus indiqué pour comptabiliser cette dépense selon les normes françaises de comptabilité, un objet nommé 'compte_description' contenant la description du compte comptable précité, ainsi qu'un tableau nommé 'repartition' indiquant en majuscules les initiales portées manuscritement en haut du document ?", "Convertis cette facture en JSON")
			}
		}

		//SEND DALL-E PROMPT
		this.target.querySelector('.generate-image-button').onclick = async () => 
		{
			document.querySelector('.gpt-chat').message.push
				(
					{
						role: "user",
						content: document.querySelector('.input.question').value,
					}
				)
			ask_dall_e(main.querySelector('.input.question').value)
		}

		//TEXT TO SPEECH
		this.target.querySelector('.tts-button').onclick = async () => 
		{
			document.querySelector('.gpt-chat').message.push
				(
					{
						role: "user",
						content: document.querySelector('.input.question').value,
					}
				)
			ask_tts(main.querySelector('.input.question').value)
		}

		//TEXT TO SPEECH
		this.target.querySelector('.o1-button').onclick = async () => 
		{
			document.querySelector('.gpt-chat').message.push
				(
					{
						role: "user",
						content: document.querySelector('.input.question').value,
					}
				)
			ask_o1(main.querySelector('.input.question').value)
		}

		//SELECT PROMPT PRESET
		this.target.querySelector('.generate-image-button').onclick = async () => 
		{
			document.querySelector('.gpt-chat').message.push
				(
					{
						role: "user",
						content: document.querySelector('.input.question').value,
					}
				)
			ask_dall_e(main.querySelector('.input.question').value)
		}

		//PROCESS VIA GPT
		async function ask(model = 'gpt-4o-mini', input, display_text)
		{
			if (!input)
				return optimusToast('Vous devez rédiger une question avant de cliquer sur envoyer', 'is-warning')

			main.querySelector('.input.question').value = ""
			main.querySelector('.input.question').focus()
			main.querySelector('.gpt-disclaimer').classList.add('is-hidden')

			const template = document.getElementById('gpt-block').content.firstElementChild.cloneNode(true)
			main.querySelector('.gpt-chat').appendChild(template)
			if (input.slice(0, 10) == 'data:image')
			{
				let img = document.createElement('img')
				img.src = input
				template.querySelector('.block-question').appendChild(img)
			}
			else
				template.querySelector('.block-question').innerText += display_text || input

			main.querySelector('.input-block').classList.add('is-hidden')

			const response = await fetch('https://api.openai.com/v1/chat/completions',
				{
					method: 'POST',
					mode: 'cors',
					headers:
					{
						'Content-Type': 'application/json',
						'Authorization': 'Bearer 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'
					},
					body: JSON.stringify({
						messages: document.querySelector('.gpt-chat').message,
						model: model,
						stream: true,
						max_tokens: 3000
					})
				})
				.then(response => response.ok ? response.body : response.json())

			if (response.error)
			{
				main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
				main.querySelector('.input-block').classList.remove('is-hidden')
				template.querySelector('.block-answer').innerText += 'Erreur : ' + response.error.message
				template.querySelector('.notification.is-success').classList.add('is-danger')
				return document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
			}

			const reader = response.getReader()

			while (true)
			{
				const { done, value } = await reader.read()
				if (done)
				{
					document.querySelector('.gpt-chat').message.push
						(
							{
								role: "assistant",
								content: template.querySelector('.block-answer').innerText,
							}
						)
					break
				}

				const textDecoder = new TextDecoder("utf-8")
				const chunk = textDecoder.decode(value)

				let incomplete_chunk = ''

				for (let line of chunk.split("\n"))
				{
					line = incomplete_chunk + line
					const trimmedLine = line.trim()
					if (!trimmedLine || trimmedLine == '' || trimmedLine === "data: [DONE]")
						continue

					let json = trimmedLine.replace("data: ", "")

					for (let obj of json.split("}{"))
					{
						try
						{
							JSON.parse(obj)
							incomplete_chunk = ''
						}
						catch (error)
						{
							incomplete_chunk = obj
							break
						}

						let deltaText = JSON.parse(obj).choices?.[0]?.delta?.content

						if (deltaText == ' ')
							deltaText = '\u00a0'

						if (deltaText)
						{
							template.querySelector('.block-answer').innerText += deltaText
							document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
						}
					}
				}
			}
			main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
			main.querySelector('.input-block').classList.remove('is-hidden')
			document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
		}

		//PROCESS VIA DALL-E-3
		async function ask_dall_e(input)
		{
			if (!input)
				return optimusToast('Vous devez rédiger une question avant de cliquer sur envoyer', 'is-warning')

			main.querySelector('.gpt-disclaimer').classList.add('is-hidden')

			const template = document.getElementById('gpt-block').content.firstElementChild.cloneNode(true)
			main.querySelector('.gpt-chat').appendChild(template)
			template.querySelector('.block-question').innerText += input

			main.querySelector('.input-block').classList.add('is-hidden')

			fetch('https://api.openai.com/v1/images/generations',
				{
					method: 'POST',
					mode: 'cors',
					headers:
					{
						'Content-Type': 'application/json',
						'Authorization': 'Bearer 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'
					},
					body: JSON.stringify({
						prompt: input,
						model: 'dall-e-3',

					})
				})
				.then(response => response.json())
				.then(response => 
				{
					main.querySelector('.input-block').classList.remove('is-hidden')
					main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
					main.querySelector('.input.question').value = ""
					main.querySelector('.input.question').focus()

					if (response.error)
					{
						response = { data: [{ revised_prompt: 'Erreur : ' + response.error.message }] }
						template.querySelector('.notification.is-success').classList.add('is-danger')
					}

					document.querySelector('.gpt-chat').message.push
						(
							{
								role: "assistant",
								content: response.data[0].revised_prompt,
							}
						)

					template.querySelector('.block-answer').innerHTML = response.data[0].revised_prompt + '<br/><br/>'
					if (response.data[0].url)
					{
						let img = document.createElement('img')
						img.src = response.data[0].url
						template.querySelector('.block-answer').appendChild(img)
					}
					document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
				})
		}

		//PROCESS VIA TTS
		async function ask_tts(input)
		{
			if (!input)
				input = main.querySelectorAll('.block-answer')[main.querySelectorAll('.block-answer').length - 1].innerText
			//return optimusToast('Vous devez rédiger un texte dans le champ avant de lancer la conversion audio', 'is-warning')

			main.querySelector('.gpt-disclaimer').classList.add('is-hidden')
			main.querySelector('.input.question').value = ""
			main.querySelector('.input.question').focus()

			const template = document.getElementById('gpt-block').content.firstElementChild.cloneNode(true)
			main.querySelector('.gpt-chat').appendChild(template)
			template.querySelector('.block-question').innerText += input
			main.querySelector('.fa-beat-fade').classList.add('fa-beat-fade')

			main.querySelector('.input-block').classList.add('is-hidden')

			const audio = document.createElement('audio')
			audio.setAttribute('controls', 'controls')

			const mediaSource = new MediaSource()
			audio.src = URL.createObjectURL(mediaSource)
			mediaSource.addEventListener('sourceopen', sourceOpen)

			async function sourceOpen()
			{
				const sourceBuffer = mediaSource.addSourceBuffer('audio/mpeg')
				audio.play()
				let response = await fetch('https://api.openai.com/v1/audio/speech',
					{
						method: 'POST',
						mode: 'cors',
						headers:
						{
							'Content-Type': 'application/json',
							'Authorization': 'Bearer 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'
						},
						body: JSON.stringify({
							input: input,
							model: 'tts-1',
							voice: 'nova'
						})
					})
					.then(response => response.ok ? response.body : response.json())

				if (response.error)
				{
					main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
					main.querySelector('.input-block').classList.remove('is-hidden')
					template.querySelector('.block-answer').innerText += 'Erreur : ' + response.error.message
					template.querySelector('.notification.is-success').classList.add('is-danger')
					return document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
				}

				template.querySelector('.block-answer').appendChild(audio)
				const reader = response.getReader()

				reader.read().then(function process({ done, value })
				{
					if (done)
					{
						if (mediaSource.readyState === 'open')
						{
							mediaSource.endOfStream()
							main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
							main.querySelector('.input-block').classList.remove('is-hidden')
							document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
						}
					}
					else
					{
						sourceBuffer.appendBuffer(value)

						sourceBuffer.addEventListener('updateend', () =>
						{
							if (!sourceBuffer.updating && mediaSource.readyState === 'open')
								reader.read().then(process)
						})
					}
				})
			}
		}

		//PROCESS VIA WHISPER
		async function ask_whisper(input)
		{
			if (!input)
				return optimusToast('Vous devez fournir un fichier audio avant de pouvoir lancer la transcription', 'is-warning')

			main.querySelector('.gpt-disclaimer').classList.add('is-hidden')
			main.querySelector('.input.question').value = ""
			main.querySelector('.input.question').focus()

			const template = document.getElementById('gpt-block').content.firstElementChild.cloneNode(true)
			main.querySelector('.gpt-chat').appendChild(template)

			template.querySelector('.block-question').innerHTML += 'Peux-tu transcrire le fichier audio suivant ?<br/>'
			let audio = document.createElement('audio')
			const fileReader = new FileReader()
			fileReader.readAsDataURL(input)
			fileReader.onload = () =>
			{
				audio.src = fileReader.result
				audio.classList.add('mt-3')
				audio.setAttribute('controls', 'controls')
				template.querySelector('.block-question').appendChild(audio)
			}

			main.querySelector('.fa-beat-fade').classList.add('fa-beat-fade')
			main.querySelector('.input-block').classList.add('is-hidden')

			const formData = new FormData()
			formData.append('file', input)
			formData.append('model', 'whisper-1')
			fetch('https://api.openai.com/v1/audio/transcriptions',
				{
					body: formData,
					method: 'POST',
					mode: 'cors',
					headers:
					{
						'Authorization': 'Bearer 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'
					},
				})
				.then(response => response.json())
				.then(response => 
				{
					if (response.error)
					{
						template.querySelector('.block-answer').innerText += 'Erreur : ' + response.error.message
						template.querySelector('.notification.is-success').classList.add('is-danger')
					}
					else
					{
						document.querySelector('.gpt-chat').message.push
							(
								{
									role: "assistant",
									content: response.text,
								}
							)
						template.querySelector('.block-answer').innerHTML += 'Voici la transcription :<br/>' + response.text
					}
					main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
					main.querySelector('.input-block').classList.remove('is-hidden')
					document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
				})
		}

		//PROCESS VIA O1
		async function ask_o1(input)
		{
			if (!input)
				return optimusToast('Vous devez rédiger une question avant de cliquer sur envoyer', 'is-warning')

			main.querySelector('.gpt-disclaimer').classList.add('is-hidden')

			const template = document.getElementById('gpt-block').content.firstElementChild.cloneNode(true)
			main.querySelector('.gpt-chat').appendChild(template)
			template.querySelector('.block-question').innerText += input

			main.querySelector('.input-block').classList.add('is-hidden')

			fetch('https://api.openai.com/v1/chat/completions',
				{
					method: 'POST',
					mode: 'cors',
					headers:
					{
						'Content-Type': 'application/json',
						'Authorization': 'Bearer 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'
					},
					body: JSON.stringify({
						messages: main.querySelector('.gpt-chat').message,
						model: 'o1-preview',
						max_completion_tokens: 10000

					})
				})
				.then(response => response.json())
				.then(response => 
				{
					main.querySelector('.input-block').classList.remove('is-hidden')
					main.querySelector('.fa-beat-fade').classList.remove('fa-beat-fade')
					main.querySelector('.input.question').value = ""
					main.querySelector('.input.question').focus()

					if (response.error)
					{
						response = { data: [{ revised_prompt: 'Erreur : ' + response.error.message }] }
						template.querySelector('.notification.is-success').classList.add('is-danger')
					}

					document.querySelector('.gpt-chat').message.push
						(
							{
								role: "assistant",
								content: response.choices[0].message.content,
							}
						)

					template.querySelector('.block-answer').innerHTML = '<pre class="language-markdown"><code class="language-markdown">' + response.choices[0].message.content + '</code></pre><br/><br/>'
					highlight_code(template.querySelector('.block-answer'))
					setTimeout(() =>
						template.querySelectorAll('span').forEach(element => 
						{
							element.innerText = element.innerText.replaceAll('#', '')
							element.innerText = element.innerText.replaceAll('**', '')
						}), 200)
					document.querySelector('.optimus-main').scrollTop = document.querySelector('.gpt-chat').scrollHeight
				})
		}
	}
}