export default class contactHuissiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		let user = await rest(this.server + '/optimus-contacts/' + this.owner + '/contacts/' + this.id, 'GET', {})

		if (user.data.address != null && user.data.city != null)
			rest(store.user.server + '/optimus-avocats/huissiers/' + user.data.city, 'GET')
				.then(huissiers =>
				{
					document.getElementById('contact_editor_tabs_container').style.height = document.body.offsetHeight - document.getElementById('contact_editor_tabs_container').offsetTop - parseInt(window.getComputedStyle(main).paddingTop) + 'px'

					huissiers = huissiers.data.ListeEtudeByInseeResult.Etude

					user.data.geoaddress = user.data.address.split("\n").filter(line => !line.toUpperCase().includes('CEDEX')).filter(line => !line.toUpperCase().startsWith('CS')) + ',' + user.data.zipcode + ',' + user.data.city_name
					fetch('https://nominatim.openstreetmap.org/search?format=json&q=' + user.data.geoaddress)
						.then(response => response.json())
						.then(async response => 
						{
							if (response.length > 0)
							{
								const mymap = await load('/components/optimus_map.js', document.getElementById('contact_editor_tabs_container'),
									{
										GoogleMaps: true,
										GoogleStreetView: true,
										StartPoint: user.data.geoaddress
									})

								mymap.leaflet.setZoom(10)

								const myIcon = L.icon({
									iconUrl: '/images/marker-icon-huissiers.png',
								})

								for (let huiss of huissiers)
								{
									const lat = huiss.lat
									const lon = huiss.lon
									const center = [lat, lon]
									const marker = L.marker(center, { icon: myIcon }).addTo(mymap.leaflet)

									const propArray = [huiss.nom, huiss.ads2, huiss.cp, huiss.ville, huiss.tel, huiss.em1]

									const list = document.createElement('div')
									list.classList.add('has-text-centered')

									for (const prop of propArray)
									{
										const p = document.createElement('p')
										if (prop == huiss.nom)
											p.classList.add('has-text-grey', 'subtitle', 'mb-0')

										p.classList.add('has-text-centered')
										p.innerText = prop
										list.appendChild(p)

									}

									const saveButton = document.createElement('div')
									saveButton.className = 'fa-solid fa-download'
									saveButton.classList.add('is-clickable')
									saveButton.addEventListener('click', () => import_huissier(huiss))
									list.appendChild(saveButton)

									marker.bindPopup(list)
								}
							}
							else
								await load('/services/optimus-avocats/contacts/no_address.html', this.target)
						})


				})
		else
			await load('/services/optimus-avocats/contacts/no_address.html', this.target)

		function import_huissier(infos)
		{
			rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts', 'GET', {
				"filter": [{
					field: "company_name",
					type: "=",
					value: infos.nom
				}]
			}).then(exists =>
			{
				if (exists.data && exists.data.length > 0)
				{
					optimusToast("Cet huissier a déjà été importé dans vos contacts !", 'is-warning')
					return false
				}

				else
					rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts', 'POST', {
						"categorie": 60,
						"type": 30,
						"lastname": '',
						"company_name": infos.nom,
						"address": infos.ads3 ? (infos.ads2 + '\n' + (infos.ads3 ? infos.ads3 + '\n' : '') + infos.cp + ' ' + infos.ville) : infos.ads2,
						"phone": infos.tel,
						"pro_fax": infos.fax,
						"pro_email": infos.em1,
						"pro_website": infos.web,
						"notes": infos.hor,
						"zipcode": infos.ads3 ? '' : infos.cp,
						"city_name": infos.ville,
						"city": infos.ads3 ? '' : infos.commune_insee
					})
						.then(() =>
						{
							optimusToast("L'huissier a été importé dans vos contacts", 'is-success')
						})
			})
		}
	}
}