export default class contactCourts
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-avocats/contacts/dossiers.html', this.target, null, true)

		let dossiers = await rest('https://' + this.server + '/optimus-avocats/' + this.owner + '/contacts/' + this.id + '/dossiers').then(response => response.data)

		if (!dossiers)
			this.target.querySelector('.no-connection').classList.remove('is-hidden')
		else
			dossiers.forEach(dossier =>
			{
				let template = document.getElementById('dossier').content.cloneNode(true)
				template.querySelector('.dossier').value = dossier.displayname
				template.querySelector('.message-header').onclick = () => router('optimus-avocats/dossiers/editor?owner=' + this.owner + '&id=' + dossier.id)
				for (let qualite of dossier.qualites)
				{
					let span = document.createElement('span')
					span.classList.add('tag', 'is-warning', 'is-rounded', 'is-medium', 'm-2')
					span.innerText = qualite.displayname
					template.querySelector('.message-body').appendChild(span)
				}
				for (let mandataire of dossier.mandataires)
				{
					let span = document.createElement('span')
					span.classList.add('tag', 'is-warning', 'is-rounded', 'is-medium', 'm-2')
					span.innerHTML = mandataire.type_displayname + ' de&nbsp;<span href="" class="has-text-link is-clickable"> ' + mandataire.mandataire_displayname + "</span>"
					span.onclick = () => router('optimus-contacts/contacts/editor?owner=' + this.owner + '&id=' + mandataire.mandataire)
					template.querySelector('.message-body').appendChild(span)
				}
				document.getElementById('dossiers-container').appendChild(template)
			})


	}
}