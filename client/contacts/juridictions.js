export default class contactCourts
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{

		await load('/services/optimus-avocats/contacts/juridictions.html', this.target, null, true)

		let itm = {}
		let authorizations = {}
		let juridictions = {}

		rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts/' + store.queryParams.id, 'GET', {})
			.then(data =>
			{
				itm = data.data
				authorizations = data.authorizations
				if (itm.city != null)
					display_juridictions()
				else
					load('/services/optimus-avocats/contacts/no_address.html', this.target)
			})

		function display_juridictions()
		{
			rest(store.user.server + '/optimus-avocats/constants/juridictions/' + itm.city, 'GET')
				.then(data =>
				{
					juridictions = data.data

					for (const [key, value] of Object.entries(juridictions))
					{
						let column = document.getElementById('juridiction').content.cloneNode(true)
						column.querySelector('.subtitle').innerHTML += value.nom.toUpperCase()
						column.querySelector('.description').innerHTML += value?.addresse1 + '<br/>'
						column.querySelector('.description').innerHTML += value?.addresse2 + '<br/>'
						column.querySelector('.description').innerHTML += value?.code_postal + ' ' + value.commune + '<br/>'
						column.querySelector('.description').innerHTML += '<br/>'
						column.querySelector('.description').innerHTML += value?.telephone ? "Tél : " + value.telephone + '<br/>' : ''
						column.querySelector('.description').innerHTML += value?.fax ? "Fax : " + value.fax + '<br/>' : '<br/>'
						column.querySelector('.description').innerHTML += value?.courriel + '<br/>'
						column.querySelector('.icon').addEventListener('click', () => import_juridictions(key))
						document.getElementById('juridictions-container').appendChild(column)
					}
				})
		}

		function import_juridictions(id)
		{
			rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts', 'GET', {
				"filter":
					[
						{
							field: "company_name",
							type: "=",
							value: juridictions[id].nom
						}
					]
			})
				.then(exists =>
				{
					if (exists.data && exists.data.length > 0)
						return optimusToast('Cette juridiction existe déjà dans vos contacts !', 'is-warning')
					else
						rest(store.user.server + '/optimus-contacts/constants/communes', 'GET', { filter: [{ field: 'commune_insee', type: '=', value: juridictions[id].commune_insee }] })
							.then(ville =>
							{
								rest(store.user.server + '/optimus-contacts/' + store.queryParams.owner + '/contacts', 'POST', {
									"categorie": 50,
									"type": 30,
									"lastname": null,
									"company_name": juridictions[id].nom,
									"address": juridictions[id].addresse1 + '\n' + (juridictions[id].addresse2 ? juridictions[id].addresse2 + '\n' : '') + juridictions[id].code_postal + ' ' + juridictions[id].commune,
									"phone": juridictions[id].telephone,
									"fax": juridictions[id].fax,
									"email": juridictions[id].courriel,
									"zipcode": ville.data[0].code_postal,
									"city_name": ville.data[0].nom,
									"city": juridictions[id].commune_insee
								})
									.then(() => optimusToast('La juridiction a été importée vos contacts', 'is-success'))
							})
				})
		}
	}
}