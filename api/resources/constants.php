<?php
include('libs/datatables.php');

$get = function ()
{
	global $connection, $resource, $input;
	
	auth();
	allowed_origins_only();
		
	$input->constant = check('id', $input->path[2], 'module', true);

	$resource = new stdClass();
	if (in_array($input->constant, array("diligences_categories", "dossiers_domaines", "intervenants_qualites", "intervenants_mandataires_types")))
	{
		$resource->id = (object) array("type" => "strictly_positive_integer", "field" => $input->constant . ".id");
		$resource->value = (object) array("type" => "string", "field" => $input->constant . ".value");
	}
	else if ($input->constant == "dossiers_sousdomaines")
	{
		$resource->id = (object) array("type" => "strictly_positive_integer", "field" => $input->constant . ".id");
		$resource->domaine = (object) array("type" => "string", "field" => $input->constant . ".domaine");
		$resource->value = (object) array("type" => "string", "field" => $input->constant . ".value");
	}
	else if ($input->constant == "diligences_subcategories")
	{
		$resource->id = (object) array("type" => "strictly_positive_integer", "field" => $input->constant . ".id");
		$resource->category = (object) array("type" => "string", "field" => $input->constant . ".category");
		$resource->value = (object) array("type" => "string", "field" => $input->constant . ".value");
	}
	else if ($input->constant == "juridictions")
	{
		$input->commune_insee = $input->path[3];
		check('commune_insee', $input->path[3], 'string', true);

		$input->body->filter = array((object)array("field" => "recherche_insee", "type" => "=", "value" => $input->commune_insee));

		if (strlen($input->commune_insee) != 5)
			return array("code" => 400, "message" => "le paramètre 'commune_insee' doit contenir 5 chiffres");
		
		$resource->id = (object) array("type" => "string", "field" => "juridictions.id");
		$resource->recherche_insee = (object) array("type" => "string", "field" => "communes_to_juridictions.commune_insee", "reference" => (object) array("db" => "common.communes_to_juridictions", "id" => "juridictions.id", "match" => "communes_to_juridictions.juridiction_id"));
		$resource->nom = (object) array("type" => "string", "field" => "juridictions.nom");
		$resource->addresse1 = (object) array("type" => "string", "field" => "juridictions.addresse1");
		$resource->addresse2 = (object) array("type" => "string", "field" => "juridictions.addresse2");
		$resource->commune_insee = (object) array("type" => "string", "field" => "juridictions.commune_insee");
		$resource->code_postal = (object) array("type" => "string", "field" => "juridictions.code_postal");
		$resource->commune = (object) array("type" => "string", "field" => "juridictions.commune");
		$resource->telephone = (object) array("type" => "string", "field" => "juridictions.telephone");
		$resource->fax = (object) array("type" => "string", "field" => "juridictions.fax");
		$resource->courriel = (object) array("type" => "email", "field" => "juridictions.courriel");
	}
	else
		return array("code" => 404, "message" => "La constante demandée n'existe pas");

	$results = datatable_request($connection, (object) $resource, 'common', $input->constant);
	$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
	$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
	
	return array("code" => 200, "data" => sanitize((object) $resource, $results), "last_row" => $last_row, "last_page" => $last_page);
};
?>