<?php
$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->dossier = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->intervenant = check('intervenant', $input->path[5], 'strictly_positive_integer', true);
	$input->body->contact = check('contact', $input->body->contact, 'strictly_positive_integer', true);
	$input->body->type = check('type', $input->body->type, 'strictly_positive_integer', true);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/'.@$input->dossier);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->dossier))
		return array("code" => 404, "message" => "Ce dossier n'existe pas");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers_intervenants', 'id', $input->intervenant))
		return array("code" => 404, "message" => "Cet intervenant n'existe pas");

	if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->body->contact))
		return array("code" => 400, "message" => "Le contact n° " . $input->body->contact . " n'existe pas");
	
	if (!exists($connection, 'common','intervenants_mandataires_types', 'id', $input->body->type))
		return array("code" => 400, "message" => "Le type n° " . $input->body->type . " n'existe pas");
	
	$mandataire_exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`dossiers_intervenants_mandataires` WHERE intervenant = '" . $input->intervenant .  "' AND contact = '" . $input->body->contact . "' AND type = '" . $input->body->type . "'");
	if ($mandataire_exists->rowCount() > 0)
		return array("code" => 400, "message" => "Un mandataire identique est déjà renseigné");

	if (!$connection->query("INSERT INTO `user_" . $input->owner . "`.`dossiers_intervenants_mandataires` SET intervenant = '" . $input->intervenant . "', contact = '" . $input->body->contact . "', type = '" . $input->body->type . "'"))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);

	$contact = $connection->query("SELECT displayname FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $input->body->contact . "'")->fetch(PDO::FETCH_OBJ);

	return array("code" => 201, "data" => array("id" => intval($connection->lastInsertId()), "contact" => $input->body->contact, "displayname" => $contact->displayname,"type" => $input->body->type));
};


$delete = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->dossier = check('dossier', $input->path[3], 'strictly_positive_integer', true);
	$input->intervenant = check('intervenant', $input->path[5], 'strictly_positive_integer', true);
	$input->mandataire = check('mandataire', $input->path[7], 'strictly_positive_integer', true);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
		if (in_array('modify', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	}
	
	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->dossier))
		return array("code" => 404, "message" => "Ce dossier n'existe pas");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers_intervenants', 'id', $input->intervenant))
		return array("code" => 404, "message" => "Cet intervenant n'existe pas");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers_intervenants_mandataires', 'id', $input->mandataire))
		return array("code" => 404, "message" => "Ce mandataire n'existe pas");

	if (!$connection->query("DELETE FROM `user_" . $input->owner . "`.`dossiers_intervenants_mandataires` WHERE id = '" . $input->mandataire . "'"))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	return array("code" => 200);
};
?>
