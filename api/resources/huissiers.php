<?php
$get = function ()
{
	global $optimus_connection, $input;

	$input->commune_insee = $input->path[2];
	check('commune_insee', $input->path[2], 'integer', true);

	if (!$input->commune_insee)
		return array("code" => 400, "message" => "le paramètre 'commune_insee' n'a pas été renseigné");
	
	if (strlen($input->commune_insee) != 5)
		return array("code" => 400, "message" => "le paramètre 'commune_insee' doit contenir 5 chiffres");
	
	$client = new SoapClient("http://euro.huissier-justice.fr/annuaire.asmx?WSDL");
	$huissiers = $client->ListeEtudeByInsee(array('insee' => $input->commune_insee));
	
	if (!$huissiers OR sizeof($huissiers->ListeEtudeByInseeResult->Etude) == 0)
		return array("code" => 404, "message" => "Aucun huissier n'a été trouvé pour ce code INSEE");
	else
		return array("code" => 200, "data" => $huissiers);
};
?>