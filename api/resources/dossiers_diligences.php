<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "dossiers_diligences.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"dossier": { "type": "positive_integer", "field": "dossiers_diligences.dossier", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"] },
	"date": { "type": "date", "field": "dossiers_diligences.date", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "' . date('Y-m-d') . '"},
	"intervenant": { "type": "positive_integer", "field": "dossiers_diligences.intervenant", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "current_user" },
	"beneficiaire": { "type": "positive_integer", "field": "dossiers_diligences.beneficiaire", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "current_user" },
	"commission": { "type": "decimal", "field": "dossiers_diligences.commission", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "0" },
	"description": { "type": "text", "field": "dossiers_diligences.description", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"category" : { "type": "positive_integer", "field": "dossiers_diligences.category",  "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "1" },
	"type" : { "type": "positive_integer", "field": "dossiers_diligences.type",  "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "1" },
	"quantity": { "type": "decimal", "field": "dossiers_diligences.quantity", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "1" },
	"price": { "type": "decimal", "field": "dossiers_diligences.price", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": "250" },
	"invoice_business_displayname": { "type": "string", "field": "dossiers_diligences.invoice_business_displayname", "post": ["ignored"], "patch": ["notnull", "notempty"], "default": null },
	"invoice_server": { "type": "string", "field": "dossiers_diligences.invoice_server", "post": ["ignored"], "patch": ["notnull", "notempty"], "default": null },
	"invoice_owner": { "type": "positive_integer", "field": "dossiers_diligences.invoice_owner", "post": ["ignored"], "patch": ["notnull", "notempty"], "default": null },
	"invoice_id": { "type": "positive_integer", "field": "dossiers_diligences.invoice_id", "post": ["ignored"], "patch": ["notnull", "notempty"], "default": null },
	"invoice_number": { "type": "string", "field": "dossiers_diligences.invoice_number", "post": ["ignored"], "patch": ["notnull", "notempty"], "default": null },
	"intervenant_displayname" : { "type": "string", "field": "users.displayname", "reference" : { "db" : "server.users", "id" : "users.id", "match" : "dossiers_diligences.intervenant" } },
	"category_description" : { "type": "string", "field": "diligences_categories.value", "reference" : { "db" : "common.diligences_categories", "id" : "diligences_categories.id", "match" : "dossiers_diligences.category" } },
	"type_description" : { "type": "string", "field": "diligences_subcategories.value", "reference" : { "db" : "common.diligences_subcategories", "id" : "diligences_subcategories.id", "match" : "dossiers_diligences.type" } }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $resource, $input;

	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', false);
	$input->subid = check('subid', $input->path[5], 'strictly_positive_integer', false);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
		if (isset($input->id))
		{
			$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
			if (in_array('read', $restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce dossier");
		}
		else
		{
			$restrictions = get_restrictions_list($input->user->id, $input->owner, 'dossiers');
			if (sizeof($restrictions) > 0 AND array_count_values(array_column($restrictions, 0))['read'] == sizeof($restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lister les dossiers de cet utilisateur");
		}
	
	//REQUETE SUR UNE DILIGENCE IDENTIFIÉE
	if (isset($input->subid))
	{
		$input->body->filter[] = (object)array("field" => "id", "type" => "=", "value" => $input->subid);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'dossiers_diligences');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cette diligence n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	//REQUETE SUR TOUTES LES DILIGENCES AU FORMAT DATATABLES
	else 
	{	
		$input->body->filter[] = (object)array("field" => "dossier", "type" => "=", "value" => $input->id);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'dossiers_diligences', $restrictions);
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		
		if (is_array($restrictions))
		{
			for ($i=0; $i < sizeof($results); $i++)
				$results[$i]['restrictions'] = $restrictions['dossiers/' . $results[$i]['id']] ?? array_diff($restrictions['dossiers'],['create']);
			$results = array_values(array_filter($results, fn ($result) => (!in_array('read', $result['restrictions']))));
		}
		
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};


$post = function ()
{
	global $connection, $resource, $input;
	
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', false);
	$input->body->dossier = $input->id;

	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->id))
		return array("code" => 404, "message" => "Le dossier mentionné dans le chemin n'existe pas");
	
	check_input_body($resource, 'post');
	
	if ($input->body->intervenant)
		if (!exists($connection, 'server', 'users', 'id', $input->body->intervenant))
			return array("code" => 400, "message" => "L'intervenant mentionné n'existe pas");
	
	if ($input->body->beneficiaire)
		if (!exists($connection, 'server', 'users', 'id', $input->body->beneficiaire))
			return array("code" => 400, "message" => "Le bénéficiaire mentionné n'existe pas");

	if (isset($input->body->commission) && ($input->body->commission < 0 OR $input->body->commission > 0.99))
		return array("code" => 400, "message" => "Le champ 'commission' doit être compris entre 0 et 0.99");

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers');
	if (in_array('create', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer une diligence dans ce dossier");
	
	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'dossiers_diligences');
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'dossiers_diligences');
		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
};


$patch = function ()
{
	global $connection, $resource, $input;

	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', false);
	$input->subid = check('subid', $input->path[5], 'strictly_positive_integer', false);
	
	check_input_body($resource, 'patch');
	
	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0) 
		return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier les diligences de ce dossier");
	
	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->id))
		return array("code" => 404, "message" => "Le dossier mentionné dans le chemin n'existe pas");
	
	$diligence = $connection->query("SELECT invoice_number FROM `user_" . $input->owner . "`.`dossiers_diligences` WHERE id = '" . $input->subid . "'")->fetch(PDO::FETCH_OBJ);
	if (!$diligence)
		return array("code" => 404, "message" => "Cette diligence n'existe pas");
	if ($diligence->invoice_number)
	{
		array_push($resource->invoice_business_displayname->patch, "immutable");
		array_push($resource->invoice_server->patch, "immutable");
		array_push($resource->invoice_owner->patch, "immutable");
		array_push($resource->invoice_id->patch, "immutable");
		array_push($resource->invoice_number->patch, "immutable");
	}

	if ($input->body->dossier)
		if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->body->dossier))
			return array("code" => 400, "message" => "Le dossier mentionné n'existe pas");

	if ($input->body->intervenant)
		if (!exists($connection, 'server', 'users', 'id', $input->body->intervenant))
			return array("code" => 400, "message" => "L'intervenant mentionné n'existe pas");

	if ($input->body->beneficiaire)
		if (!exists($connection, 'server', 'users', 'id', $input->body->beneficiaire))
			return array("code" => 400, "message" => "Le bénéficiaire mentionné n'existe pas");
	
	if (isset($input->body->commission) && ($input->body->commission < 0 OR $input->body->commission > 0.99))
		return array("code" => 400, "message" => "Le champ 'commission' doit être compris entre 0 et 0.99");

	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'dossiers_diligences', $input->subid);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->subid . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'dossiers_diligences');
		return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
};


$delete = function ()
{
	global $connection, $input;
	
	auth();
	allowed_origins_only();
	
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->subid = check('subid', $input->path[5], 'strictly_positive_integer', false);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
	if (in_array('delete', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour supprimer des diligences dans ce dossier");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->id))
		return array("code" => 404, "message" => "Le dossier renseigné dans le chemin n'existe pas");

	$diligence = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`dossiers_diligences` WHERE id = " . $input->subid)->fetch(PDO::FETCH_ASSOC);
	if (!$diligence)
		return array("code" => 404, "message" => "Cette diligence n'existe pas");

	if ($diligence['invoice_id'] != '')
		return array("code" => 400, "message" => "Cette diligence ne peut pas être supprimée car elle a déjà été facturée");

	$diligence_delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.dossiers_diligences WHERE id = '" . $input->subid . "'");
	return array("code" => 200);
};
?>