<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "dossiers_intervenants.id", "post": ["ignored"], "default": 0 },
	"dossier": { "type": "strictly_positive_integer", "field": "dossiers_intervenants.dossier", "post": ["required", "notnull", "notempty"], "default": 0 },
	"contact": { "type": "strictly_positive_integer", "field": "dossiers_intervenants.contact", "post": ["required", "notnull", "notempty"], "default": "0" },
	"qualites": { "type": "string", "field": "dossiers_intervenants.id", "vitrual": true },
	"mandataires": { "type": "string", "field": "dossiers_intervenants.id", "vitrual": true },
	"displayname" : { "type": "string", "field": "contacts.displayname", "reference" : { "db" : "user_{owner}.contacts", "id" : "contacts.id", "match" : "dossiers_intervenants.contact" } }
}
', null, 512, JSON_THROW_ON_ERROR);


$get = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->subid = check('subid', $input->path[5], 'strictly_positive_integer', false);
	$resource->displayname->reference->db = str_replace('{owner}', $input->owner, $resource->displayname->reference->db);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce dossier");
	}
	
	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->id))
		return array("code" => 404, "message" => "Ce dossier n'existe pas");

	//REQUETE SUR UN INTERVENANT IDENTIFIÉ
	if (isset($input->subid))
	{
		$input->body = json_decode('{"references": true, "filter": [{"field": "dossier", "type": "=", "value": ' . $input->id . '},{"field": "id", "type": "=", "value": ' . $input->subid . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'dossiers_intervenants');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cet intervenant n'existe pas");
		else
		{
			$results[0]['qualites'] = array();
			$results[0]['qualites'] = array();
			$qualites = $connection->query("SELECT qualite FROM `user_" . $input->owner . "`.`dossiers_intervenants_qualites` WHERE intervenant = '" . $input->subid . "'");
			while ($qualite = $qualites->fetch(PDO::FETCH_ASSOC))
				array_push($results[0]['qualites'], intval($qualite['qualite']));

			$results[0]['mandataires'] = array();
			$mandataires = $connection->query("SELECT id, contact, type FROM `user_" . $input->owner . "`.`dossiers_intervenants_mandataires` WHERE intervenant = '" . $results[0]['id'] . "'");
			while ($mandataire = $mandataires->fetch(PDO::FETCH_ASSOC))
			{
				$displayname = $connection->query("SELECT displayname FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $mandataire['contact'] . "'")->fetch(PDO::FETCH_ASSOC);
				array_push($results[0]['mandataires'], array('id'=>intval($mandataire['id']),'contact'=>intval($mandataire['contact']), 'type'=>intval($mandataire['type']), 'displayname'=>$displayname['displayname']));
			}

			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
		}
	}
	//REQUETE SUR TOUS LES INTERVENANTS AU FORMAT DATATABLES
	else 
	{	
		$input->body = json_decode('{"references": true, "filter": [{"field": "dossier", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'dossiers_intervenants', $restrictions);
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		for ($i=0; $i < sizeof($results); $i++)
		{
			unset($results[$i]['dossier']);
			if (!$input->body->columns || in_array('qualites',$input->body->columns))
			{
				$results[$i]['qualites'] = array();
				$qualites = $connection->query("SELECT qualite FROM `user_" . $input->owner . "`.`dossiers_intervenants_qualites` WHERE intervenant = '" . $results[$i]['id'] . "'");
				while ($qualite = $qualites->fetch(PDO::FETCH_ASSOC))
					array_push($results[$i]['qualites'], intval($qualite['qualite']));
			}

			if (!$input->body->columns || in_array('mandataires',$input->body->columns))
			{
				$results[$i]['mandataires'] = array();
				$mandataires = $connection->query("SELECT id, contact, type FROM `user_" . $input->owner . "`.`dossiers_intervenants_mandataires` WHERE intervenant = '" . $results[$i]['id'] . "'");
				while ($mandataire = $mandataires->fetch(PDO::FETCH_ASSOC))
				{
					$displayname = $connection->query("SELECT displayname FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $mandataire['contact'] . "'")->fetch(PDO::FETCH_ASSOC);
					array_push($results[$i]['mandataires'], array('id'=>intval($mandataire['id']), 'contact'=>intval($mandataire['contact']), 'type'=>intval($mandataire['type']), 'displayname'=>$displayname['displayname']));
				}
			}
		}
		if (is_array($restrictions))
			$results = array_values(array_filter($results, fn ($result) => (!in_array('read', $result['restrictions']))));
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};


$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$resource->displayname->reference->db = str_replace('{owner}', $input->owner, $resource->displayname->reference->db);
	
	$input->body->dossier = $input->id;
	
	$qualites = $input->body->qualites;
	unset($resource->qualites);
	foreach ($input->body->qualites as $qualite)
	{
		check('qualite', $qualite, 'strictly_positive_integer', true);
		if (!exists($connection, 'common','intervenants_qualites', 'id', $qualite))
			return array("code" => 400, "message" => "La qualite n° " . $qualite . " n'existe pas");
	}

	$mandataires = $input->body->mandataires;
	unset($resource->mandataires);
	foreach ($input->body->mandataires as $mandataire)
	{
		check('contact', $mandataire->contact, 'strictly_positive_integer', true);
		if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $mandataire->contact))
			return array("code" => 400, "message" => "Le contact n° " . $mandataire->contact . " n'existe pas");
		check('type', $mandataire->type, 'strictly_positive_integer', true);
		if (!exists($connection, 'common','intervenants_mandataires_types', 'id', $mandataire->type))
			return array("code" => 400, "message" => "Le type n° " . $mandataire->type . " n'existe pas");
	}

	check_input_body($resource, 'post');

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/'.@$input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");

	$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/'.@$input->body->contact);
	if (in_array('read', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce contact");

	if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->body->contact))
		return array("code" => 400, "message" => "Le contact n° " . $input->body->contact . " n'existe pas");

	$intervenant_exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`dossiers_intervenants` WHERE dossier = '" . $input->id . "' AND contact = '" . $input->body->contact . "'");
	if ($intervenant_exists->rowCount() > 0)
		return array("code" => 400, "message" => "Cet intervenant existe déjà dans le dossier");
	
	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'dossiers_intervenants');
	if ($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'dossiers_intervenants');
		
		$results[0]['qualites'] = $qualites;
		foreach ($qualites as $qualite)
			$connection->query("INSERT INTO `user_" . $input->owner . "`.`dossiers_intervenants_qualites` SET intervenant = '" . $results[0]['id'] . "', qualite = '" . $qualite . "'");
		
		$results[0]['mandataires'] = array();
		foreach ($mandataires as $mandataire)
		{
			$connection->query("INSERT INTO `user_" . $input->owner . "`.`dossiers_intervenants_mandataires` SET intervenant = '" . $results[0]['id'] . "', contact = '" . $mandataire->contact . "', type = '" . $mandataire->type . "'");
			$mandataire->id = $connection->lastInsertId();
			$displayname = $connection->query("SELECT displayname FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $mandataire->contact . "'")->fetch(PDO::FETCH_ASSOC);
			$mandataire->displayname = $displayname['displayname'];
			$results[0]['mandataires'][] = (array)$mandataire;
		}
		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$delete = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->subid = check('subid', $input->path[5], 'strictly_positive_integer', false);

	check_input_body($resource, 'delete');

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	}
	
	$intervenant_exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`dossiers_intervenants` WHERE id = " . $input->subid, PDO::FETCH_OBJ);
	if ($intervenant_exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cet intervenant n'existe pas");
	
	if (!$connection->query("DELETE FROM `user_" . $input->owner . "`.`dossiers_intervenants` WHERE id = " . $input->subid))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	return array("code" => 200);
};
?>
