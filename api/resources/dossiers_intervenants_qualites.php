<?php
$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->dossier = check('id', $input->path[3], 'strictly_positive_integer', true);
	$input->intervenant = check('intervenant', $input->path[5], 'strictly_positive_integer', true);
	$input->qualite = check('qualite', $input->path[7], 'strictly_positive_integer', false);

	if ($input->qualite)
		$input->body->qualites = array($input->qualite);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/'.@$input->dossier);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->dossier))
		return array("code" => 404, "message" => "Ce dossier n'existe pas");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers_intervenants', 'id', $input->intervenant))
		return array("code" => 404, "message" => "Cet intervenant n'existe pas");

	foreach ($input->body->qualites as $qualite)
	{
		check('qualite', $qualite, 'strictly_positive_integer', true);
		if (!exists($connection, 'common','intervenants_qualites', 'id', $qualite))
			return array("code" => 404, "message" => "La qualite n° " . $qualite . " n'existe pas");
		$qualite_exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`dossiers_intervenants_qualites` WHERE intervenant = '" . $input->intervenant .  "' AND qualite = '" . $input->qualite . "'");
		if ($qualite_exists->rowCount() > 0)
			return array("code" => 400, "message" => "Cet intervenant dispose déjà de la qualité n°" . $qualite);
	}

	foreach ($input->body->qualites as $qualite)
		if (!$connection->query("INSERT INTO `user_" . $input->owner . "`.`dossiers_intervenants_qualites` SET intervenant = '" . $input->intervenant . "', qualite = '" . $qualite . "'"))
			return array("code" => 400, "message" => $connection->errorInfo()[2]);

	return array("code" => 201);
};


$delete = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->dossier = check('dossier', $input->path[3], 'strictly_positive_integer', true);
	$input->intervenant = check('intervenant', $input->path[5], 'strictly_positive_integer', true);
	$input->qualite = check('qualite', $input->path[7], 'strictly_positive_integer', true);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
		if (in_array('modify', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	}
	
	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->dossier))
		return array("code" => 404, "message" => "Ce dossier n'existe pas");

	if (!exists($connection, 'user_' . $input->owner, 'dossiers_intervenants', 'id', $input->intervenant))
		return array("code" => 404, "message" => "Cet intervenant n'existe pas");

	$qualite_exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`dossiers_intervenants_qualites` WHERE intervenant = '" . $input->intervenant .  "' AND qualite = '" . $input->qualite . "'");
	if ($qualite_exists->rowCount() == 0)
		return array("code" => 404, "message" => "Cet intervenant ne dispose pas de cette qualité");
	
	if (!$connection->query("DELETE FROM `user_" . $input->owner . "`.`dossiers_intervenants_qualites` WHERE intervenant = '" . $input->intervenant .  "' AND qualite = '" . $input->qualite . "'"))
		return array("code" => 400, "message" => $connection->errorInfo()[2]);
	
	return array("code" => 200);
};
?>
