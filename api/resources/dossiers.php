<?php
use optimus\OptimusWebsocket;
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "strictly_positive_integer", "field": "dossiers.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"nom": { "type": "filename", "field": "dossiers.nom"},
	"numero": { "type": "string", "field": "dossiers.numero", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["emptytonull"], "default": "auto" },
	"date_ouverture": { "type": "date", "field": "dossiers.date_ouverture", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "default": "' . date('Y-m-d') . '" },
	"date_classement": { "type": "date", "field": "dossiers.date_classement", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"numero_archive": { "type": "positive_integer", "field": "dossiers.numero_archive", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"domaine": { "type": "positive_integer", "field": "dossiers.domaine", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"sous_domaine": { "type": "positive_integer", "field": "dossiers.sous_domaine", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"conseil": { "type": "boolean", "field": "dossiers.conseil", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"contentieux": { "type": "boolean", "field": "dossiers.contentieux", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"aj": { "type": "boolean", "field": "dossiers.aj", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 0 },
	"notes": { "type": "text", "field": "dossiers.notes", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"rg": { "type": "string", "field": "dossiers.rg", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"portalis": { "type": "string", "field": "dossiers.portalis", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"parquet": { "type": "string", "field": "dossiers.parquet", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"instruction": { "type": "string", "field": "dossiers.instruction", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"domaine_description" : { "type": "string", "field": "dossiers_domaines.value", "reference" : { "db" : "common.dossiers_domaines", "id" : "dossiers_domaines.id", "match" : "dossiers.domaine" } },
	"sous_domaine_description" : { "type": "string", "field": "dossiers_sousdomaines.value", "reference" : { "db" : "common.dossiers_sousdomaines", "id" : "dossiers_sousdomaines.id", "match" : "dossiers.sous_domaine" } },
	"displayname" : { "type" : "string", "field": "dossiers.displayname", "virtual": true }
}
', null, 512, JSON_THROW_ON_ERROR);


$get = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();
		
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', false);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
		if (isset($input->id))
		{
			$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
			if (in_array('read', $restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce dossier");
		}
		else
		{
			$restrictions = get_restrictions_list($input->user->id, $input->owner, 'dossiers');
			if (sizeof($restrictions) > 0 AND array_count_values(array_column($restrictions, 0))['read'] == sizeof($restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lister les dossiers de cet utilisateur");
		}

	//REQUETE SUR UN DOSSIER IDENTIFIÉ
	if (isset($input->id))
	{
		$input->body = json_decode('{"references": true, "filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'dossiers');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Ce dossier n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	//REQUETE SUR TOUS LES DOSSIERS AU FORMAT DATATABLES
	else 
	{	
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'dossiers', $restrictions);
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		if (is_array($restrictions))
		{
			for ($i=0; $i < sizeof($results); $i++)
				$results[$i]['restrictions'] = $restrictions['dossiers/' . $results[$i]['id']] ?? array_diff((array)$restrictions['dossiers'],['create']);
			$results = array_values(array_filter($results, fn ($result) => (!in_array('read', $result['restrictions']))));
		}
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};


$post = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	check_input_body($resource, 'post');
	
	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers');
	if (in_array('create', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer un dossier chez cet utilisateur");

	
	if ($input->body->numero == 'auto')
	{
		$last_numero = $connection->query("SELECT numero FROM `user_" . $input->owner . "`.`dossiers` WHERE numero LIKE '" . date('y') . "/%' ORDER BY id DESC LIMIT 1")->fetchObject();
		@$input->body->numero = date('y') . '/' . str_pad(intval(substr($last_numero->numero,3))+1, 4, "0", STR_PAD_LEFT);
	}

	if(isset($input->body->nom) AND exists($connection, 'user_' . $input->owner, 'dossiers', 'nom', $input->body->nom))
		return array("code" => 409, "message" => "Un dossier portant ce nom existe déjà dans la base");
	
	if (isset($input->body->nom) AND is_dir('/srv/files/' . get_user_email($input->owner) . '/dossiers/'.$input->body->nom))
		return array("code" => 409, "message" => "Un dossier portant ce nom existe déjà sur le cloud");
	
	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'dossiers');
	$query->execute();
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$new_dossier = datatable_request($connection, $resource, 'user_' . $input->owner, 'dossiers');

		mkdir('/srv/files/' . get_user_email($input->owner) . '/dossiers/'. $new_dossier[0]['displayname'], 0750, true);
		
		$imap_server = "{optimus-mail:143}";
		$mbox =  imap_open($imap_server . 'INBOX', get_user_email($input->owner), get_user_password($input->owner));
		if (imap_createmailbox($mbox, $imap_server . "dossiers/" . mb_convert_encoding($new_dossier[0]['displayname'], "UTF7-IMAP", "UTF8")))
		{
			imap_subscribe($mbox, $imap_server . "dossiers/" . mb_convert_encoding($new_dossier[0]['displayname'], "UTF7-IMAP", "UTF8"));
			$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
			$websocket->send(json_encode(array("command" => "mail_folders_updated", "user" => $input->owner)));
		}
		imap_close($mbox);

		return array("code" => 201, "data" => sanitize($resource, array_merge($new_dossier[0], ['restrictions' => $restrictions])));
	}
};


$patch = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	
	check_input_body($resource, 'patch');

	if (count(array_intersect(array_keys((array)$input->body),array_keys(array_filter((array)$resource, function($item){return !in_array('immutable', (array)$item->patch);})))) == 0) 
		return array("code" => 400, "message" => "Aucune donnée à modifier n'a été transmise");

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
	if (in_array('write', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier ce dossier");
	
	if (!exists($connection, 'user_' . $input->owner, 'dossiers', 'id', $input->id))
		return array("code" => 404, "message" => "Ce dossier n'existe pas");

	if (isset($input->body->nom))
		$old_name = $connection->query("SELECT displayname FROM `user_" . $input->owner . "`.dossiers WHERE id = '" . $input->id . "'")->fetchObject();
	
	if (@$old_name->displayname != @$input->body->nom)
	{
		$same_name_exists = $connection->prepare("SELECT displayname FROM `user_" . $input->owner . "`.dossiers WHERE displayname = :displayname");
		$same_name_exists->bindParam(':displayname', $input->body->nom);
		$same_name_exists->execute();
		if ($same_name_exists->rowCount() > 0)
			return array("code" => 403, "message" => "Un dossier portant ce nom existe déjà");
	}

	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'dossiers', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'dossiers');
		if ($old_name->displayname != $results[0]['displayname'])
		{
			@rename('/srv/files/' . get_user_email($input->owner) . '/dossiers/' . $old_name->displayname, '/srv/files/' . get_user_email($input->owner) . '/dossiers/' . $results[0]['displayname']);
			
			$imap_server = "{optimus-mail:143}";
			$mbox =  imap_open($imap_server . 'INBOX', get_user_email($input->owner), get_user_password($input->owner));
			if(imap_renamemailbox($mbox, $imap_server . "dossiers/" . mb_convert_encoding($old_name->displayname, "UTF7-IMAP","UTF8"), $imap_server . "dossiers/" . mb_convert_encoding($results[0]['displayname'], "UTF7-IMAP", "UTF8")))
			{
				imap_unsubscribe($mbox, $imap_server . "dossiers/" . mb_convert_encoding($old_name->displayname, "UTF7-IMAP", "UTF8"));
				imap_subscribe($mbox, $imap_server . "dossiers/" . mb_convert_encoding($new_dossier[0]['displayname'], "UTF7-IMAP", "UTF8"));
				$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
				$websocket->send(json_encode(array("command" => "mail_folders_updated", "user" => $input->owner)));
			}
			imap_close($mbox);
		}

		return array("code" => 200, "data" => sanitize($resource, array_merge((array)$results[0], ['restrictions' => $restrictions])));
	}
};


$delete = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();
	
	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);

	$restrictions = get_restrictions($input->user->id, $input->owner, 'dossiers/' . $input->id);
	if (in_array('delete', $restrictions))
		return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour supprimer ce dossier");
	
	$exists = $connection->query("SELECT displayname FROM `user_" . $input->owner . "`.`dossiers` WHERE id = '" . $input->id . "'");
	if ($exists->rowCount() == 0)
		return array("code" => 404, "message" => "Ce dossier n'existe pas");
	else
		$dossier = $exists->fetchObject();

	$exists = $connection->query("SELECT id FROM `user_" . $input->owner . "`.`dossiers_diligences` WHERE dossier = '" . $input->id . "' AND invoice_id IS NOT NULL");
	if ($exists->rowCount() > 0)
		return array("code" => 400, "message" => "Ce dossier ne peut pas être supprimé car il contient des diligences facturées");

	$dossier_delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.`dossiers` WHERE id = '" . $input->id . "'");
	$diligences_delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.dossiers_diligences WHERE dossier = '" . $input->id . "'");
	$intervenants_delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.dossiers_intervenants WHERE dossier = '" . $input->id . "'");
	$authorization_delete = $connection->query("DELETE FROM server.authorizations WHERE owner = '" . $input->owner . "' AND resource = 'dossiers/" . $input->id . "'");
	$users = $connection->query('SELECT id FROM server.users');
	while ($user = $users->fetchObject())
		$calendars_events_extended_properties_delete = $connection->query("DELETE FROM `user_" . $user->id . "`.`calendars_events_properties` WHERE property = 'optimus_avocats_dossier_id'");

	if ($dossier->displayname)
	{
		system('rm -R "/srv/files/' . get_user_email($input->owner) . '/dossiers/' . $dossier->displayname . '"');

		$imap_server = "{optimus-mail:143}";
		$mbox =  imap_open($imap_server . 'INBOX', get_user_email($input->owner), get_user_password($input->owner));
		imap_unsubscribe($mbox, $imap_server . "dossiers/" . mb_convert_encoding($dossier->displayname, "UTF7-IMAP", "UTF8"));
		imap_deletemailbox($mbox, $imap_server . "dossiers/" . mb_convert_encoding($dossier->displayname, "UTF7-IMAP", "UTF8"));
		imap_close($mbox);
		$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
		$websocket->send(json_encode(array("command" => "mail_folders_updated", "user" => $input->owner)));
	}
	return array("code" => 200);
};
?>