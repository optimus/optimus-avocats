<?php
$get = function ()
{
	global $connection, $resource, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->contact = check('contact', $input->path[3], 'strictly_positive_integer', true);

	if ($input->user->id != $input->owner AND !is_admin($input->user->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'contacts/' . $input->contact);
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire ce contact");
	}
	
	if (!exists($connection, 'user_' . $input->owner, 'contacts', 'id', $input->contact))
		return array("code" => 404, "message" => "Ce contact n'existe pas");

	$displaynames = $connection->query("SELECT id, value FROM common.intervenants_qualites")->fetchAll(PDO::FETCH_OBJ);
	foreach($displaynames as $displayname)
		$qualites_displaynames[$displayname->id] = $displayname->value;

	
	$intervenants = $connection->query("SELECT id, dossier FROM `user_" . $input->owner . "`.`dossiers_intervenants` WHERE contact = '" . $input->contact . "'")->fetchAll(PDO::FETCH_OBJ);
	foreach($intervenants as $intervenant)
	{
		$dossier = $connection->query("SELECT id, displayname FROM `user_" . $input->owner . "`.`dossiers` WHERE id = '" . $intervenant->dossier . "'")->fetch(PDO::FETCH_OBJ);
		$intervenant_qualites = $connection->query("SELECT qualite FROM `user_" . $input->owner . "`.`dossiers_intervenants_qualites` WHERE intervenant = '" . $intervenant->id . "'")->fetchAll(PDO::FETCH_OBJ);
		$qualites = array();
		foreach($intervenant_qualites as $qualite)
			$qualites[] = array('qualite' => $qualite->qualite, 'displayname' => $qualites_displaynames[$qualite->qualite]);
		
		$interventions[$dossier->id] = array(
			'id' => $dossier->id,
			'displayname' => $dossier->displayname,
			'qualites' => $qualites,
			'mandataires' => array()
		);
	}

	
	$displaynames = $connection->query("SELECT id, value FROM common.intervenants_mandataires_types")->fetchAll(PDO::FETCH_OBJ);
	foreach($displaynames as $displayname)
		$mandataires_types_displaynames[$displayname->id] = $displayname->value;

	$mandataires = $connection->query("SELECT id, intervenant, type FROM `user_" . $input->owner . "`.`dossiers_intervenants_mandataires` WHERE contact = '" . $input->contact . "'")->fetchAll(PDO::FETCH_OBJ);
	foreach($mandataires as $mandataire)
	{
		$intervenant = $connection->query("SELECT contact, dossier FROM `user_" . $input->owner . "`.`dossiers_intervenants` WHERE id = '" . $mandataire->intervenant . "'")->fetch(PDO::FETCH_OBJ);
		$dossier = $connection->query("SELECT id, displayname FROM `user_" . $input->owner . "`.`dossiers` WHERE id = '" . $intervenant->dossier . "'")->fetch(PDO::FETCH_OBJ);
		$mandataire_detail = $connection->query("SELECT contact FROM `user_" . $input->owner . "`.`dossiers_intervenants` WHERE id = '" . $mandataire->intervenant . "'")->fetch(PDO::FETCH_OBJ);
		$mandataire_detail = $connection->query("SELECT id, displayname FROM `user_" . $input->owner . "`.`contacts` WHERE id = '" . $mandataire_detail->contact . "'")->fetch(PDO::FETCH_OBJ);
		if (!$interventions[$dossier->id])
			$interventions[$dossier->id] = array(
				'id' => $dossier->id,
				'displayname' => $dossier->displayname,
				'qualites' => array(),
				'mandataires' => array()
			);
		$interventions[$dossier->id]['mandataires'][] = array(
					'type' => $mandataire->type,
					'type_displayname' => $mandataires_types_displaynames[$mandataire->type],
					'mandataire' => $mandataire_detail->id,
					'mandataire_displayname' => $mandataire_detail->displayname
		);
	}

	$interventions = array_values($interventions);
	usort($interventions, fn($a, $b) => strcmp(strtolower($a['displayname']), strtolower($b['displayname'])));

	return array("code" => 200, "data" => $interventions);
};
?>
